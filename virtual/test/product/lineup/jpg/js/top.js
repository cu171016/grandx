$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  $('#topTitleSection').fadeTo(0, 0);
  $('#topTitleSection #topTitleArea').fadeTo(0, 0);

  $('#topStyleSection').fadeTo(0, 0);
  $('#topStyleSection #topStyleArea').fadeTo(0, 0);

  $('#topStorySection').fadeTo(0, 0);
  $('#topStorySection #topStoryArea').fadeTo(0, 0);

  $('#topSpecSection').fadeTo(0, 0);
  $('#topSpecSection #topSpecArea').fadeTo(0, 0);
  $('#topSpecSection ul').fadeTo(0, 0);

  $('#topLocalNaviSection').fadeTo(0, 0);
  $('#topLocalNaviSection ul li').fadeTo(0, 0);
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){

    $('#topTitleSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#topTitleSection').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#topTitleSection #topTitleArea').fadeTo(750, 1);
            setTimeout(function(){ GL.bDeffed01 = true; }, 550);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#topStyleSection #topStyleArea p.btnDetail').one('inview', function(event, isInView){

      if(isInView){

        GL.bDeffed01Timer = setInterval(function(){

          if(!GL.bDeffed01){
            return;
          }
          else{
            GL.bDeffed01 = false;
            clearInterval(GL.bDeffed01Timer);
          }

          var deferred = $.Deferred();
          deferred
            .then(function(){
              var d = $.Deferred();
              $('#topStyleSection').fadeTo(750, 1);
              setTimeout(function(){ d.resolve(); }, 550);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#topStyleSection #topStyleArea').fadeTo(750, 1);
              setTimeout(function(){ d.resolve(); }, 0);
              return d;
            });
          deferred.resolve();

        }, 100);

      }

    });

    $('#topStorySection #topStoryArea p.btnDetail').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#topStorySection').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#topStorySection #topStoryArea').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#topSpecSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#topSpecSection').delay(500).fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 700);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#topSpecSection #topSpecArea').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 650);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#topSpecSection ul').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#topLocalNaviSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#topLocalNaviSection').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 500);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#topLocalNaviSection ul li:eq(0)').fadeTo(600, 1);
            $('#topLocalNaviSection ul li:eq(1)').delay(300).fadeTo(600, 1);
            $('#topLocalNaviSection ul li:eq(2)').delay(600).fadeTo(600, 1);
            $('#topLocalNaviSection ul li:eq(3)').delay(900).fadeTo(600, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 背景拡大アニメーションhoverイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $('p#topTitleBtn a, p.btnDetail a').hover(
    function(){
      $(this).parent('p').parent('div').next('p').addClass('currentHover');
    },
    function(){
      $(this).parent('p').parent('div').next('p').removeClass('currentHover');
    }
  );

});
