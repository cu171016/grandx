$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  var CO = {};  // 定数用配列
  var GL = {};  // グローバル変数用配列

  // ユーザーエージェント情報
  GL.ua = {};

  // SPサイズ
  CO.iImgSpSize = 768;

  // SP対象
  GL.bSp = true;

  // リサイズ用タイマー
  GL.iRTimer = false;

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;

//ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
// イベントハンドラ
//ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー  

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  $('#storyTitleSection #storyMvArea p').fadeTo(0, 0);

  $('#storyHotSection #storyHot01Area h3').fadeTo(0, 0);
  $('#storyHotSection #storyHot02Area').fadeTo(0, 0);
  $('#storyHotSection ul li').fadeTo(0, 0);
  $('#storyHotSection #storyHot03Area p').fadeTo(0, 0);

  $('#storyInnovationSection #storyInnovation01Area h3').fadeTo(0, 0);
  $('#storyInnovationSection #storyInnovation02Area').fadeTo(0, 0);
  $('#storyInnovationSection #storyInnovation03Area').fadeTo(0, 0);
  $('#storyInnovationSection #storyInnovation04Area').fadeTo(0, 0);

  $('#storyBestSection #storyBest01Area h3').fadeTo(0, 0);
  $('#storyBestSection ul li').fadeTo(0, 0);
  $('#storyBestSection #storyBest02Area p').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);

  if(GL.ua.isIE && GL.ua.ver <= 8){
    // IE8以下の場合
    // レスポンシブ対象外
    GL.bSp = false;
  }
  else{
    GL.bSp = true;
  }

  // ウィンドウ幅取得
  var iWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

  if(iWindowWidth < CO.iImgSpSize){
    
    // テキストと画像の位置をSP用に入れ替え
    $('#storyHotSection').append($('#storyHotSection ul'));

  }

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){

    $('#storyTitleSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#storyTitleSection #storyMvArea p').fadeTo(300, 1);
            setTimeout(function(){ GL.bDeffed01 = true; }, 500);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#storyHotSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        
        var iWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if(iWindowWidth < CO.iImgSpSize){
          
          GL.bDeffed01Timer = setInterval(function(){

            if(!GL.bDeffed01){
              return;
            }
            else{
              GL.bDeffed01 = false;
              clearInterval(GL.bDeffed01Timer);
            }

            deferred
              .then(function(){
                var d = $.Deferred();
                $('#storyHotSection #storyHot01Area h3').fadeTo(300, 1);
                setTimeout(function(){ d.resolve(); }, 550);
                return d;
              })
              .then(function(){
                var d = $.Deferred();
                $('#storyHotSection #storyHot02Area').fadeTo(300, 1);
                setTimeout(function(){ d.resolve(); }, 550);
                return d;
              })
              .then(function(){
                var d = $.Deferred();
                $('#storyHotSection #storyHot03Area p').fadeTo(500, 1);
                $('#storyHotSection ul li:eq(0)').delay(600).fadeTo(500, 1);
                $('#storyHotSection ul li:eq(1)').delay(800).fadeTo(500, 1);
                $('#storyHotSection ul li:eq(2)').delay(1000).fadeTo(500, 1);
                setTimeout(function(){ d.resolve(); }, 0);
                return d;
              });
              
          }, 100);
            
        }
        else{
          deferred
            .then(function(){
              var d = $.Deferred();
              $('#storyHotSection #storyHot01Area h3').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 550);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyHotSection #storyHot02Area').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 550);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyHotSection ul li:eq(0)').delay(300).fadeTo(300, 1);
              $('#storyHotSection ul li:eq(1)').delay(600).fadeTo(300, 1);
              $('#storyHotSection ul li:eq(2)').delay(900).fadeTo(300, 1);
              $('#storyHotSection #storyHot03Area p').delay(1500).fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 0);
              return d;
            });
        }
        deferred.resolve();
      }

    });

    $('#storyInnovationSection #storyInnovation02Area').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();

        var iWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if(iWindowWidth < CO.iImgSpSize){

          deferred
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation01Area h3').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 550);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation02Area').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 550);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation03Area').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 550);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation04Area').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 0);
              return d;
            });
          
        }
        else{

          deferred
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation01Area h3').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 700);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation02Area').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 700);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation03Area').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 700);
              return d;
            })
            .then(function(){
              var d = $.Deferred();
              $('#storyInnovationSection #storyInnovation04Area').fadeTo(300, 1);
              setTimeout(function(){ d.resolve(); }, 0);
              return d;
            });

        }
        
        deferred.resolve();
      }

    });

    $('#storyBestSection #storyBest01Area').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#storyBestSection #storyBest01Area h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#storyBestSection ul li:eq(0)').fadeTo(300, 1);
            $('#storyBestSection ul li:eq(1)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 650);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#storyBestSection #storyBest02Area p').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#inPageBnSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#inPageBnSection ul li').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // resizeイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー       
  $(window).resize(function(){

    if(GL.iRTimer !== false){
      clearTimeout(GL.iRTimer);
    }

    GL.iRTimer = setTimeout(function(){

      // ウィンドウ幅取得
      var iWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

      if(iWindowWidth < CO.iImgSpSize){

        // テキストと画像の位置をSP用に入れ替え
        $('#storyHotSection').append($('#storyHotSection ul'));

      }
      else{

        // テキストと画像の位置をPC用に入れ替え
        $('#storyHot02Area').after($('#storyHotSection ul'));

      }

    }, 0);

  });
  
});