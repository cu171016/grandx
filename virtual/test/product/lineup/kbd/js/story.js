$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  $('#storyTitleSection p#storyTitleTxt02').fadeTo(0, 0);

  $('#story01Section h3').fadeTo(0, 0);
  $('#story01Section ul.storyImgList').fadeTo(0, 0);
  $('#story01Section .storyTxtArea').fadeTo(0, 0);

  $('#story02Section h3').fadeTo(0, 0);
  $('#story02Section ul.storyImgList').fadeTo(0, 0);
  $('#story02Section .storyTxtArea').fadeTo(0, 0);
  $('#story02Section p.storyImg').fadeTo(0, 0);

  $('#story03Section h3').fadeTo(0, 0);
  $('#story03Section .storyTxtArea').fadeTo(0, 0);
  $('#story03Section p.storyImg').fadeTo(0, 0);

  $('#storyJointSection').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){
    
    $('#storyTitleSection p#storyTitleTxt02').one('inview', function(event, isInView){

      if(isInView){

        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#storyTitleSection p#storyTitleTxt02').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story01Section h3').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story01Section h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#story01Section ul.storyImgList:eq(0)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#story01Section .storyTxtArea:eq(0)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story01Section ul.storyImgList:eq(1)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story01Section ul.storyImgList:eq(1)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#story01Section .storyTxtArea:eq(1)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story02Section h3').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story02Section h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#story02Section ul.storyImgList:eq(0)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#story02Section .storyTxtArea:eq(0)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story02Section ul.storyImgList:eq(1)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story02Section ul.storyImgList:eq(1)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#story02Section .storyTxtArea:eq(1)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story02Section p.storyImg:eq(0)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story02Section p.storyImg:eq(0)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story02Section .storyTxtArea:eq(2)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story02Section .storyTxtArea:eq(2)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story02Section p.storyImg:eq(1)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story02Section p.storyImg:eq(1)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story02Section .storyTxtArea:eq(3)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story02Section .storyTxtArea:eq(3)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story03Section h3').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story03Section h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#story03Section p.storyImg:eq(0)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story03Section .storyTxtArea:eq(0)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story03Section .storyTxtArea:eq(0)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#story03Section p.storyImg:eq(1)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#story03Section p.storyImg:eq(1)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#storyJointSection').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#storyJointSection').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#inPageBnSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#inPageBnSection ul li').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });

});
