$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;
    
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  $('#styleTitleSection #styleMvArea p').fadeTo(0, 0);
  $('#styleContentsSection p#mainTxt').fadeTo(0, 0);

  $('#styleContentsSection #styleBakeArea .styleTitleInner').fadeTo(0, 0);
  $('#styleContentsSection #styleBakeArea p#styleBakeImg').fadeTo(0, 0);
  $('#styleContentsSection #styleBakeArea #styleBakeInner').fadeTo(0, 0);

  $('#styleContentsSection #styleKneadArea').fadeTo(0, 0);
  
  $('#styleContentsSection #styleFermentationArea .styleTitleInner').fadeTo(0, 0);
  $('#styleContentsSection #styleFermentationArea p#styleFermentationImg').fadeTo(0, 0);

  $('#styleContentsSection #styleDevelopmentArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleDevelopmentArea #styleDevelopmentInner').fadeTo(0, 0);
  $('#styleContentsSection #styleDevelopmentArea ul#styleDevelopmentImgList').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);
  $('#contentsWrap p#introBtn').fadeTo(0, 0);
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){
    
    $('#styleTitleSection').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleTitleSection #styleMvArea p').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleBakeArea').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleBakeArea .styleTitleInner').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleBakeArea p#styleBakeImg').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleBakeArea #styleBakeInner').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleKneadArea p').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleKneadArea').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleFermentationArea p#styleFermentationImg').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleFermentationArea .styleTitleInner').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleFermentationArea p#styleFermentationImg').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleDevelopmentArea #styleDevelopmentInner').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection p#mainTxt').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleDevelopmentArea h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleDevelopmentArea #styleDevelopmentInner').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleDevelopmentArea ul#styleDevelopmentImgList').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleDevelopmentArea > p').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#inPageBnSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#inPageBnSection ul li').fadeTo(300, 1);
            $('#contentsWrap p#introBtn').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });

});
