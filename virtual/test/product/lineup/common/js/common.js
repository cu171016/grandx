$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ロールオーバー設定
  setRollover();

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // ページ内リンクclickイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $('a[href^="anc#"]').click(function(){

    var $targetId = $($(this).attr('href').replace('anc', ''));
    
    if($targetId.length === 0){
      return false;
    }

    $('html, body').animate({scrollTop:$targetId.offset().top - $('header').height() - 30}, 750, 'easeOutCirc');

    return false;

  });

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 背景拡大アニメーションhoverイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $('#topNaviSection ul li a, #topLocalNaviSection ul li a, #inPageBnSection ul li a').hover(
    function(){
      $(this).next('span').addClass('currentHover');
    },
    function(){
      $(this).next('span').removeClass('currentHover');
    }
  );

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // プライベートメソッド
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // ロールオーバー設定メソッド
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  function setRollover(){

    $('a img').each(function(){

      if($(this).attr('src').match('_off.')){

        $(this).on('mouseover', function(){
          if(!$(this).hasClass('current')){
            $(this).attr('src', $(this).attr('src').replace('_off.', '_on.'));
          }
        });

        $(this).on('mouseout', function(){
          if(!$(this).hasClass('current')){
            $(this).attr('src', $(this).attr('src').replace('_on.', '_off.'));
          }
        });
        
      }

    });

  }

});

(function($){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // パブリックメソッド
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // ユーザーエージェント取得メソッド
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $.fn.getUaInfo = function(){
    
    var ua = {};

    // ユーザーエージェントを小文字に変換して取得
    ua.name = window.navigator.userAgent.toLowerCase();
    
    // 判定基準設定
    ua.isIE = (ua.name.indexOf('msie') >= 0 || ua.name.indexOf('trident') >= 0);
    ua.isiPhone = ua.name.indexOf('iphone') >= 0;
    ua.isiPod = ua.name.indexOf('ipod') >= 0;
    ua.isiPad = ua.name.indexOf('ipad') >= 0;
    ua.isiOS = (ua.isiPhone || ua.isiPod || ua.isiPad);
    ua.isAndroid = ua.name.indexOf('android') >= 0;
    ua.isTablet = (ua.isiPad || (ua.isAndroid && ua.name.indexOf('mobile') < 0));
    ua.isSP = (ua.isiPhone || ua.isiPod || ua.isAndroid);
  
    if(ua.isIE){
      
      // IEの場合
      // バージョン取得
      ua.verArray = /(msie|rv:?)\s?([0-9]{1,})([\.0-9]{1,})/.exec(ua.name);
      if(ua.verArray){
        ua.ver = parseInt(ua.verArray[2], 10);
      }
      
    }
    
    return ua;
    
  };

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 要素高さ揃えメソッド
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $.fn.alignHeight = function(rCnt){

    var cNo;      // 列番
    var cHeight;  // アイテムの高さ
    var rMaxH;    // 行内での高さ最大値
    var cCnt;     // 対象要素数
    var columns;  // 一列保持用配列

    $.each($(this), function(){

      // $(this)を対象にeach
      // 対象要素のheightのCSSプロパティと属性を削除
      $(this).css('height', '');
      $(this).attr('height', '');

    });

    // 並べる対象アイテム数取得
    cCnt = $(this).length - 1;

    $.each($(this), function(iCnt){ 

      // $(this)を対象にeach
      // 指定した列数の余りから列番を取得
      cNo = iCnt % rCnt;

      if(cNo === 0){

        // 列の最初の場合
        // 配列を初期化
        columns = [];

      }

      // アイテムを配列に代入
      columns[cNo] = $(this);

      // 高さ取得
      cHeight = Number($(this).css('height').replace('px', ''));

      if(cHeight > rMaxH || cNo === 0 ){

        // 取得した高さが最大値より高いもしくは1列目の場合
        // その行の最大値として代入
        rMaxH = cHeight;

      }

      if(iCnt === cCnt || cNo === rCnt - 1){

        // 列の最後もしくは全体の最後の要素の場合
        // 高さを最大値で揃える
        $.each(columns, function(){
          // columnsを対象にeach
          $(this).css('height', rMaxH);
        });

      }
    });
  };

  $.fn.alignHeight2 = function(targetEle, rCnt){

    $.each($(this), function(){

      var cNo;      // 列番
      var cHeight;  // アイテムの高さ
      var rMaxH;    // 行内での高さ最大値
      var cCnt;     // 対象要素数
      var columns;  // 一列保持用配列

      var $contents = $(this).find(targetEle);

      $.each($contents, function(){

        // $(this)を対象にeach
        // 対象要素のheightのCSSプロパティと属性を削除
        $(this).css('height', '');
        $(this).attr('height', '');

      });

      // 並べる対象アイテム数取得
      cCnt = $contents.length - 1;

      $.each($contents , function(iCnt){ 

        // $(this)を対象にeach
        // 指定した列数の余りから列番を取得
        cNo = iCnt % rCnt;

        if(cNo === 0){

          // 列の最初の場合
          // 配列を初期化
          columns = [];

        }

        // アイテムを配列に代入
        columns[cNo] = $(this);

        // 高さ取得
        cHeight = Number($(this).css('height').replace('px', ''));

        if(cHeight > rMaxH || cNo === 0 ){

          // 取得した高さが最大値より高いもしくは1列目の場合
          // その行の最大値として代入
          rMaxH = cHeight;

        }

        if(iCnt === cCnt || cNo === rCnt - 1){

          // 列の最後もしくは全体の最後の要素の場合
          // 高さを最大値で揃える
          $.each(columns, function(){ 

            // columnsを対象にeach
            $(this).css('height', rMaxH);

          });
          
        }

      });

    });

  };
  
})($);