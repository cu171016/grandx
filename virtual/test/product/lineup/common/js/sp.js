$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  var CO = {};  // 定数用配列
  var GL = {};  // グローバル変数用配列

  // ユーザーエージェント情報
  GL.ua = {};

  // SPサイズ
  CO.iImgSpSize = 641;

  // SP対象
  GL.bSp = true;

  // リサイズ用タイマー
  GL.iRTimer = false;

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  if(GL.ua.isIE && GL.ua.ver <= 8){
    // IE8以下の場合
    // レスポンシブ対象外
    GL.bSp = false;
  }
  else{
    GL.bSp = true;
  }

  if(GL.ua.isTablet){
    // タブレットの場合、
    // viewport変更
    $('meta[name=viewport]').attr('content', 'width=1200,user-scalable=no');
  }

  // ウィンドウ幅取得
  var iWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

  if(iWindowWidth < CO.iImgSpSize){
    $('body').addClass('spDevice');
  }

  // スマホ用表示調整
  changeSpStyle();

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // resizeイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).resize(function(){

    if(GL.iRTimer !== false){
      clearTimeout(GL.iRTimer);
    }

    GL.iRTimer = setTimeout(function(){

      // スマホ用表示調整
      changeSpStyle();

      // ウィンドウ幅取得
      var iWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

      if(iWindowWidth < CO.iImgSpSize){
        $('body').addClass('spDevice');
      }
      else{
        $('body').removeClass('spDevice');
      }

    }, 0);

  });
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // プライベートメソッド
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // スマホ用表示調整メソッド
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  function changeSpStyle(){

    if(GL.bSp){

      // ウィンドウ幅取得
      var iWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

      if(iWindowWidth >= CO.iImgSpSize){

        // 規定ウィンドウ幅以上の場合

        // 画像をPC用に変更    
        $('img').each(function(){
          if($(this).attr('src').indexOf('_sp') !== -1){
            $(this).attr('src', $(this).attr('src').replace('_sp', '_pc'));
          }
        });

      }
      else if(iWindowWidth < CO.iImgSpSize){

        // 規定ウィンドウ幅未満の場合

        // 画像をSP用に変更    
        $('img').each(function(){
          if($(this).attr('src').indexOf('_pc') !== -1){
            $(this).attr('src', $(this).attr('src').replace('_pc', '_sp'));
          }
        });

      }

    }

  }

});