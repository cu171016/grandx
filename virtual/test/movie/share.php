<?php

$pid_str = $_GET["pid"];
$pid = intval($pid_str);
$ogp = "https://www.grandx.jp/assets/images/movie/tvcm/share/photo" . ( $pid + 1 ) . ".jpg";

?>
<!DOCTYPE HTML>
<html>
<head>

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX ,グランエックス,タイガー魔法瓶,土鍋圧力IH炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー,ムービー,WEBムービー,グランエックスのきほん">
<meta name="description" content="佐々木希さん出演中！グランエックスTVCM | タイガー魔法瓶 TVCM">


<meta name="twitter:card" content="photo" />
<meta name="twitter:site" content="TVCM｜GX GRANDX クラブ｜タイガー魔法瓶" />
<meta name="twitter:title" content="TVCM｜GX GRANDX クラブ｜タイガー魔法瓶">
<meta name="twitter:image" content="<?php echo $ogp ?>">
<meta name="twitter:description" content="佐々木希さん出演中！グランエックスTVCM | タイガー魔法瓶 TVCM">

<meta property="og:type" content="website">
<meta property="og:title" content="TVCM｜GX GRANDX クラブ｜タイガー魔法瓶">
<meta property="og:description" content="佐々木希さん出演中！グランエックスTVCM | タイガー魔法瓶 TVCM">
<meta property="og:image" content="<?php echo $ogp ?>">
<meta property="og:site_name" content="TVCM｜GX GRANDX クラブ｜タイガー魔法瓶">

<title>TVCM｜動画ムービー｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<meta http-equiv="refresh" content="3;URL=/movie/tvcm.html">

</head>
<body>
</body>
</html>