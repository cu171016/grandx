
;(function( $, window ) {

	var $win;
	var $nav;
	var HEADER_HEIGHT = 124;
	var isSP = false;
	var OPEN_MODAL = 'open_modal';
	var CLOSE_MODAL = 'close_modal';
	var MODAL_READY = 'modal_ready';
	var current_scrollY;
	var player;
	var player_modal;
	var PLAYER_READY = 'player_ready';
	var PLAY_COMPLETE = 'play_complete';
	
	main();
	
	function main()
	{
		$(document).ready( init );
	}
	
	function init()
	{
		$win = $( window );
		$nav = $('.nav-wrapper');
		initHeader ();
		initAnchor ();
		initNav();
		initSearch();
		$win.on( 'resize', onResize );
		onResize();
		initFooter();
		setShareURL();
		if ( $('#yt_player_modal').length ) initYTMovie();
		if ( $('.wow').length ) new WOW().init();
		if ( $('.cover').length ) initCover();
		if ( $('.top').length ) new sceneTop();
		if ( $('.hover-animation').length ) initScrollHoverAction();
		if ( $('.movie').length ) new partModal ();
		if ( $('.tag_search').length ) new tagSearch ();

		if ( $('.carousel').length ) initCarousel();
		if ( $('.p-nozomi-carousel').length ) initSpNOZOMI ();

		if ( $('.cp2').length ) new sceneCP2();
		if ( $('.p-rcp-h2-coverTitle').length ) new initRcpTitle();
	}

	function initRcpTitle (){
		var str = $('.main-copy').text();
		if ( str.length == 3 ) setRcpTitle ( str, 3, 11 );
		if ( str.length == 4 ) setRcpTitle ( str, 3, 11 );
		if ( str.length == 5 ) setRcpTitle ( str, 3, 11 );
		if ( str.length == 6 ) setRcpTitle ( str, 3, 11 );
		if ( str.length == 7 ) setRcpTitle ( str, 4, 8.7 );
		if ( str.length == 8 ) setRcpTitle ( str, 4, 8.7 );
		if ( str.length == 9 ) setRcpTitle ( str, 5, 6.8 );
		if ( str.length == 10 ) setRcpTitle ( str, 5, 6.8 );
		if ( str.length >= 11 ) setRcpTitle ( str, 6, 6 );
	}

	function setRcpTitle ( str, num, size ){
		var res = str.substr(0,num) + "<br>　" + str.substr(num,str.length);
		$('.main-copy').html(res);
		$('.main-copy').css( { 'font-size': size + 'rem' } );
	}


	function setShareURL (){
		if ( $('.btn-facebook').length ) {
			var url = "https://www.grandx.jp" + location.pathname;
			$('.btn-facebook').attr('href', 'http://www.facebook.com/share.php?u=' + url );
		}
	}

	function tagSearch () {

		var q = location.hash.replace('#', '');

		$.ajax({
	        url:'/assets/data/list_article.xml',
	        type:'GET',
	        dataType:'xml',
	        timeout:2000,
	        error:function() {
	            //alert("ロード失敗");
	        },
	        success:function(xml){
	            parse(xml, q);
	        }
	    });

	    function parse (xml, q){
	    	var $tag = $(xml).find("tag[id=" + q + "]");

	    	if ( $tag.length == 0 ) {
	    		// 該当するタグなし
	    		$('.min-txt').text('「' + q + '」に該当するタグはありませんでした。');
	    		return;
	    	}

	    	$('.tag-title').text( $tag.attr('name') );

	    	var $container = $('.contents-section');
	    	var $ul = $('<ul class="thumb-container flex"></ul>');
	    	var $tmp = $('<ul></ul>');
	    	$container.append( $ul );
	    	var $list = $(xml).find("." + q);
	    	if ( $list.length < 3 ) $ul.addClass('fixed');

	    	if ( $list.length == 0 ) {
	    		// 該当記事なし
	    		$('.min-txt').html('Coming Soon<br>近日中に公開予定です。');
	    		return;
	    	}

	    	$list.each(function(idx, elm) {
	    		
	    		$tmp.append( $(elm) );
	    	});
	    	$ul.append( $tmp.html() );

	    	$ul.find('li').each(function(idx, elm) {
	    		$(elm).addClass('thumb-block size-medium hover-animation');
	    	});
	    }
	}

	function initSpNOZOMI (){

		if ( $('.p-nozomi-carousel--index').length ) {
			var slick_idx = $('.p-nozomi-carousel--index').slick({
				infinite: false,
				dots: false,
				slidesToShow: 5,
				slidesToScroll: 1,
				autoplay:false,
				arrows:true,
				speed:600,
				responsive: [
					{
						breakpoint: 768, //ブレークポイント3の値
						settings: "unslick"
		          	}
	      		]
			});
		} else {
			var slick = $('.p-nozomi-carousel').slick({
				infinite: false,
				dots: false,
				slidesToShow: 5,
				slidesToScroll: 1,
				autoplay:false,
				arrows:true,
				speed:600,
				responsive: [
					{
						breakpoint: 768,
						settings: { slidesToShow: 3 }
					}
				]
			});
		}
	}

	function initCarousel (){

		var slick = $('.carousel-inner').slick({
			infinite: true,
			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay:false,
			arrows:true,
			speed:600
		});

		var $thumbList = $('.cd-modal-trigger');

		$thumbList.on ( 'click', function (){
			var index = $thumbList.index(this);
			slick.slick('slickGoTo', index );
		});

		slick.on('beforeChange', function(event, slick, currentSlide, nextSlide){
			setFB_Props( nextSlide );
			//setTW_Props( nextSlide );
			$('.btn-twitter-m').attr('data-idx',nextSlide );
		});
		$('.btn-twitter-m').on( 'click', function(){
			var idx = $(this).attr('data-idx');
			var t = "佐々木希さん出演中！グランエックスTVCM | タイガー魔法瓶 TVCM";
			var u = "https://www.grandx.jp/movie/share.php?pid=" + idx;
			t = encodeURIComponent(t);
			u = encodeURIComponent(u);
			window.open(this.href, 'facebook_share', 'width=550, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1').location.href = ("https://twitter.com/share?url=" + u + "&text=" + t + "&count=none&lang=ja");
		});
	}

	function setFB_Props ( idx ){
		var baseURL = "http://www.facebook.com/share.php?u=";
		var shareURL = "https://www.grandx.jp/movie/share.php?pid=" + idx;
		shareURL = encodeURIComponent(shareURL);
		$('.btn-facebook-m').attr('href', baseURL + shareURL);
	}

	function setTW_Props ( idx ){
		var d = "佐々木希さん出演中！グランエックスTVCM | タイガー魔法瓶 TVCM";
		var baseURL = "http://twitter.com/share?count=horizontal&amp;";
		var shareURL = "https://www.grandx.jp/movie/share.php?pid=" + idx;
		var txt = "text=" + encodeURIComponent(d) + "&amp;url=";
		shareURL = encodeURIComponent(shareURL);
		$('.btn-twitter-m').attr('href', baseURL + txt + shareURL);
	}

	function initYTMovie (){
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	}

	function YouTubeIframeAPIReady( vID) {
		player_modal = new YT.Player('yt_player_modal', {
			width: '1920',
			height: '1080',
			videoId: vID,
			playerVars: {
				rel: "0"
			},
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		});
	}

	function onPlayerReady (event){
		player_modal.setPlaybackQuality ('default');
	}
	function onPlayerStateChange(event) {
		var ytStatus = event.data;
	    if (ytStatus == YT.PlayerState.ENDED) {
	        $(window).trigger( new $.Event( PLAY_COMPLETE, {} ) );
	    }
	}

	function initScrollHoverAction (){
		var $thumb = $('.hover-animation');
		
		$(window).on('scroll', checkHoverInit );
		checkHoverInit();

		function checkHoverInit(){
			if ( isSP ) {
				var scroll = $(this).scrollTop();
				$thumb.each(function(index, el) {
					var top = $(this).offset().top - 200;
					if ( scroll > top + 100 ) {
						$(this).removeClass('active');
					} else if ( scroll > top ){
						$(this).addClass('active');
					} else {
						$(this).removeClass('active');
					}
				});
			}
		}
	}

	function initCover () {
		var headerH = $('.header').height();
		var top = $('.main-content').offset().top - headerH;
		
		$(window).on('scroll', function() {
			var scroll = $(this).scrollTop();
			var per = 1 - ( scroll / top );
			if ($(this).scrollTop() > top) per = 0;
			$('.cover').css("opacity",per );
		});
	}

	function initFooter (){

		function pagePluginCode(w) {
	        // 幅に応じて高さを変更する場合
	        if(w > 400) {
	            var h = 300;
	        } else {
	            var h = 200;
	        }
	        return '<div class="fb-page" data-href="https://www.facebook.com/g.x.tiger/?fref=ts" data-tabs="timeline" data-width="' + w + '" data-height="' + h + '" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/g.x.tiger/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/g.x.tiger/?fref=ts">GRAND X（グランエックス）</a></blockquote></div>';
	    }
	 
	    // ページプラグインを追加する要素
	    var facebookWrap = $('.block-facebook');
	    var fbBeforeWidth = ''; // 前回変更したときの幅
	    var fbWidth = facebookWrap.width(); // 今回変更する幅
	    var fbTimer = false;
	    $(window).on('load resize', function() {
	        if (fbTimer !== false) {
	            clearTimeout(fbTimer);
	        }
	        fbTimer = setTimeout(function() {
	            fbWidth = facebookWrap.width(); // 変更後の幅を取得
	            // 前回の幅から変更があった場合のみ処理
	            // スマホだとスクロール時にリサイズが発生することがあるため
	            if(fbWidth != fbBeforeWidth) {
	                facebookWrap.html(pagePluginCode(fbWidth)); // ページプラグインのコード変更
	                window.FB.XFBML.parse(); // ページプラグインの再読み込み
	                fbBeforeWidth = fbWidth; // 今回変更分を保存しておく
	            }
	        }, 200);
	    });

	    getInstagramData();
	}

	function getInstagramData(){

		var userFeed = new Instafeed({
			target: 'insta_list',
			get: 'user', //ユーザーから取得
			userId: '5922365235', //ユーザーID(先ほど確認した'user_id')
			sortBy: 'most-recent',//最新記事から順に取得
			links: true , //画像リンク取得
			limit: 6, //取得する画像数を設定
			resolution: 'low_resolution', //画像サイズを設定
			template: '<li><a href="{{link}}" target="_blank"><img src="{{image}}"></a></li>',
			accessToken: '5922365235.25dd279.45e67935d9b74b1eb684a7ed1dbe4da7' //アクセストークン
		});
		userFeed.run();

	}

	function sceneTop (){

		init();

		function init(){
			$('.accordion-inner').hide();
			$('.h2-title').on( 'click', toggleMessage );
			$('.btn-message-toggl').on( 'click', toggleMessage );

			new MainImg();
			// SP ////////////
			setCarouselBanner();
			new partNews( '#newsContainer', true );
		}

		function toggleMessage (){
			$('.message-container').toggleClass('open');
			$('.accordion-inner').slideToggle();

			if ( $('.message-container').hasClass('open') ) {
				ga('send','event','top','click','メッセージ＞オープン');
			} else {
				ga('send','event','top','click','メッセージ＞クローズ');
			}
		}
	}

	function sceneCP2 (){

		init();

		function init(){
			$('.toggle').hide();
			$('.h2-msg-title').on( 'click', toggleMessage );
			$('.btn-message-toggl').on( 'click', toggleMessage );
		}

		function toggleMessage (){
			$('.nozomi-msg').toggleClass('open');
			$('.toggle').slideToggle();
		}
	}

	
	var easeSpeed = 800;
	var interval = 5800;
	

	function MainImg (){
		var stageWidth = 1200;
		var stage = new createjs.Stage("main-carousel");
		var queue = new createjs.LoadQueue();
		var currentNum = 0;
		var manifest = [];
		var navList = [];
		var linkList = [];
		var tagList = [];
		var timer;
		
		var currentBtn;
		stage.enableMouseOver();

		var mainArea = new createjs.Container();
		mainArea.x = 100;
		stage.addChild( mainArea );
		var mainImg = new slideContainer( mainArea, stageWidth );
		initLinkBtn();

		var backArea = new createjs.Container();
		backArea.x = -500;
		backArea.id = -1;
		var backImg = new slideContainer( backArea, stageWidth );
		initArrowBtn( backArea );

		var nextArea = new createjs.Container();
		nextArea.x = 700;
		nextArea.id = 1;
		var nextImg = new slideContainer( nextArea, stageWidth );
		initArrowBtn( nextArea );


		var btnContainer = new createjs.Container();
		btnContainer.y = 436;
		stage.addChild( btnContainer );

		createMask();
		stage.addChild( backArea );
		stage.addChild( nextArea );


		initImage ();
		
		function initImage (){

			var $imgList = $('.slide-list').find('img');
			$imgList.each(function(idx, el) {
				var obj = {};
				obj.id = idx;
				obj.src = $(this).attr('src');
				manifest.push( obj );
				linkList.push( $(this).attr('data-href') );
				tagList.push( $(this).attr('data-tagmanager') );
			});
			
			queue.loadManifest(manifest, false);
			queue.addEventListener("fileload", fileloadHandler);
			queue.addEventListener("complete", completeHandler);
			queue.load();
		}

		function fileloadHandler(evt) {
			
			mainImg.addImage(evt);
			backImg.addImage(evt);
			nextImg.addImage(evt);
			createNavBtn( evt.item.id );
		}

		function completeHandler(evt) {
			queue.removeEventListener("fileload", fileloadHandler);
			queue.removeEventListener("complete", completeHandler);
			createjs.Ticker.setFPS(60);
			createjs.Ticker.addEventListener("tick", stage);

			backImg.moveToRightInit(-1);
			nextImg.moveToLeft(1);

			startAutoPlay();
			setCurrentBtn();
			createOverlay();
		}

		function createOverlay (){
			var back_overlay = new createjs.Shape();
			back_overlay.graphics.beginFill("#000000");
			back_overlay.graphics.moveTo(0, 0);
			back_overlay.graphics.lineTo(100, 0);
			back_overlay.graphics.lineTo(223, 250);
			back_overlay.graphics.lineTo(100, 500);
			back_overlay.graphics.lineTo(0, 500);
			back_overlay.alpha = 0.5;
			stage.addChild(back_overlay);

			var next_overlay = new createjs.Shape();
			next_overlay.graphics.beginFill("#000000");
			next_overlay.graphics.moveTo(1300, 0);
			next_overlay.graphics.lineTo(1400, 0);
			next_overlay.graphics.lineTo(1400, 500);
			next_overlay.graphics.lineTo(1300, 500);
			next_overlay.graphics.lineTo(1179, 250);
			next_overlay.alpha = 0.5;
			stage.addChild(next_overlay);
		}

		function createMask (){
			var main_mask = new createjs.Shape();
			main_mask.graphics.beginFill("#ffffff");
			main_mask.graphics.drawRect(0, 0, 1200, 500);
			main_mask.x = 100;
			mainArea.mask = main_mask;

			var back_BG = new createjs.Shape();
			back_BG.graphics.beginFill("#ffffff");
			back_BG.graphics.moveTo(0, 0);
			back_BG.graphics.lineTo(100, 0);
			back_BG.graphics.lineTo(223, 250);
			back_BG.graphics.lineTo(100, 500);
			back_BG.graphics.lineTo(0, 500);
			stage.addChild(back_BG);
			back_BG.x = 2;

			var back_mask = new createjs.Shape();
			back_mask.graphics.beginFill("#ffffff");
			back_mask.graphics.moveTo(0, 0);
			back_mask.graphics.lineTo(100, 0);
			back_mask.graphics.lineTo(223, 250);
			back_mask.graphics.lineTo(100, 500);
			back_mask.graphics.lineTo(0, 500);
			backArea.mask = back_mask;


			var next_BG = new createjs.Shape();
			next_BG.graphics.beginFill("#ffffff");
			next_BG.graphics.moveTo(1300, 0);
			next_BG.graphics.lineTo(1400, 0);
			next_BG.graphics.lineTo(1400, 500);
			next_BG.graphics.lineTo(1300, 500);
			next_BG.graphics.lineTo(1179, 250);
			stage.addChild(next_BG);
			next_BG.x = -2;

			var next_mask = new createjs.Shape();
			next_mask.graphics.beginFill("#ffffff");
			next_mask.graphics.moveTo(1300, 0);
			next_mask.graphics.lineTo(1400, 0);
			next_mask.graphics.lineTo(1400, 500);
			next_mask.graphics.lineTo(1300, 500);
			next_mask.graphics.lineTo(1179, 250);
			nextArea.mask = next_mask;
		}

		function initArrowBtn ( target, _id ){
			target.cursor = "pointer";
		    target.addEventListener('click', function(e){
		    	var p = currentNum + Number( e.currentTarget.id );
		        setSlideImage( p );
		    });
		}

		function initLinkBtn (){
			mainArea.cursor = "pointer";
			mainArea.addEventListener('click', function(e){
				ga('send','event','top','click','メインバナー＞' + tagList[ currentNum ]);
		        location.href = linkList[ currentNum ];
		    });
		}

		function createNavBtn ( _id ){
			var btn = new createjs.Shape();
			btn.graphics.beginFill("#8b989e");
			btn.graphics.drawCircle(5, 5, 5);
			btn.graphics.beginFill("#ffffff");
			btn.graphics.drawCircle(5, 5, 4);
			btn.id = _id;
			btn.x = Number(_id) * 25;
			btn.cursor = "pointer";
			btnContainer.addChild( btn );
			navList.push(btn);

			btnContainer.x = ( stageWidth - btn.x ) / 2 + 100;

			var rectangle = new createjs.Rectangle(0, 0, 10, 10);
			btn.cache(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

			btn.addEventListener('rollover', function(e){
		        btnOver ( e.target );
		    });
		    btn.addEventListener('rollout', function(e){
		        if ( currentBtn != e.target ) btnOut ( e.target );
		    });
		    btn.addEventListener('click', function(e){
		        setSlideImage( e.target.id );
		    });
		}

		function btnOver ( target ){
			target.filters = [ new createjs.ColorFilter(0, 0, 0, 1, 29, 138, 168, 0) ];
		    target.updateCache();
		}

		function btnOut ( target ){
			target.filters = [];
		    target.updateCache();
		}

		function setCurrentBtn (){
			if ( currentBtn ) {
				currentBtn.mouseEnabled = true;
				btnOut ( currentBtn );
			}
			currentBtn = navList[ currentNum ];
			btnOver ( currentBtn );
			currentBtn.mouseEnabled = false;
		}

		function startAutoPlay (){
			timer = setInterval( function (){
				slideImageLeft( 1 );
			}, interval );
		}

		function setSlideImage ( _id ){
			var num = _id - currentNum;
			clearInterval( timer );
			startAutoPlay();
			if ( num > 0 ) {
				slideImageLeft( num );
			} else {
				slideImageRight( num );
			}
		}

		function slideImageLeft ( num ){
			currentNum += num;
			if ( currentNum > manifest.length - 1 ) currentNum = 0;
			setCurrentBtn();
			mainImg.slideImageLeft( num );
			backImg.slideImageLeft( num );
			nextImg.slideImageLeft( num );
		}
		function slideImageRight ( num ){
			currentNum += num;
			if ( currentNum < 0 ) currentNum = manifest.length - 1;
			setCurrentBtn();
			mainImg.slideImageRight( num );
			backImg.slideImageRight( num );
			nextImg.slideImageRight( num );
		}
	}

	
	function slideContainer ( target, _w ){
		var stageWidth =  _w;
		var objList = [];
		var container = this;

		init();

		function init () {
			
		}

		this.mainArea = target;

		this.addImage = function (evt){
			var bitmap = new createjs.Bitmap(evt.result);
			var obj = new createjs.Container ();
			obj.x = stageWidth * Number( evt.item.id );
			obj.addChild(bitmap);
			objList.push( obj );
			this.mainArea.addChild( obj );
		};

		this.moveToLeft = function ( num ){
			for ( var p = 0 ; p < num ; p++ ){
				var removeObj = objList.shift();
				objList.push( removeObj );
			}
			this.setPosition( 0 );
		}

		this.moveToRight = function ( num ){
			for ( var p = 0 ; p < Math.abs(num) ; p++ ){
				var removeObj = objList.pop();
				objList.unshift( removeObj );
			}
			this.setPosition( Math.abs(num) );
		}
		this.moveToRightInit = function ( num ){
			for ( var p = 0 ; p < Math.abs(num) ; p++ ){
				var removeObj = objList.pop();
				objList.unshift( removeObj );
			}
			this.setPosition( 0 );
		}

		this.setPosition = function ( p ){
			for ( var i = 0 ; i < objList.length ; i++) {
				var obj = objList[i];
				obj.x = stageWidth * (i-p);
			}
		}

		this.slideImageLeft = function ( num ){
			for ( var i = 0 ; i < objList.length ; i++) {
				var obj = objList[i];
				var endX = ( i - num ) * stageWidth;

				if ( i == 0 ) {
					new createjs.Tween.get(obj)
					.to( { x: endX }, easeSpeed, createjs.Ease.sineInOut )
					.call( function (){
						container.moveToLeft( num );
					});
				} else {
					new createjs.Tween.get(obj)
					.to( { x: endX }, easeSpeed, createjs.Ease.sineInOut );
				}
			}
		};

		this.slideImageRight = function ( num ){
			this.moveToRight( num );

			for ( var i = 0 ; i < objList.length ; i++) {
				var obj = objList[i];
				var endX = i * stageWidth;
				new createjs.Tween.get(obj)
				.to( { x: endX }, easeSpeed, createjs.Ease.sineInOut );
			}
		};
	}

	//-----------------------------------------------------------------
	// partNews
	//-----------------------------------------------------------------
	function partNews( target, flag )
	{
		var VISIBLE_TIME = 3000; // msec
		var ANIMATE_SPAN = 50; // msec
		var ANIMATE_SPEED = 1; // px
		
		var $win = $( window );
		var $base = $( '#news' );
		var $wrap = $base.find(target).find( 'ul' );
		var $news = $wrap.children();
		var $current;
		var $prev;
		
		var current = 0;
		var max = $news.length;
		var left = 0;
		var timer;
		_init();
		
		function _init()
		{
			$base.hide();
			$news.hide();
			//$win.one( CONTENT_START_EVT, _onContentStart );
			//$win.on( ALL_READY_EVT, _onShow );
			_onContentStart();
			_onShow();
		}
		
		function _onContentStart( _e )
		{
			setTimeout( _onNewsStart, 900 );
		}
		
		function _onNewsStart()
		{
			_showCurrentNews();
		}
		
		function _showCurrentNews()
		{
			left = 0;
			$current = $news.eq( current );
			$current.css({ 'left': 0, 'top': '100%' }).show()
			.animate({ 'top': '0%' },{ 'duration': 300, 'complete': _onCurrentNewsStart });
		}
		
		function _onCurrentNewsStart()
		{
			if ( $current.width() > $wrap.width() ) {
				timer = setTimeout( function (){ _animateNews() }, VISIBLE_TIME / 2 );
			} else {
				timer = setTimeout( function (){ _showNextNews() }, VISIBLE_TIME );
			}
		}
		
		function _animateNews()
		{
			left += ANIMATE_SPEED;
			
			if ( $current.width() - left < $wrap.width() ) {
				timer = setTimeout( function (){ _showNextNews() }, VISIBLE_TIME / 2 );
			} else {
				$current.css({ 'left': - left });
				timer = setTimeout( function (){ _animateNews() }, ANIMATE_SPAN );
			}
		}
		
		function _showNextNews()
		{
			if ( max <= 1 ) return;
			$prev = $current;
			$prev.animate({ 'top': '-100%' },{ 'duration': 300, 'complete': _onPrevNewsEnd });
			current = ( current < max-1 ) ? current + 1 : 0;
		}
		
		function _onPrevNewsEnd()
		{
			$prev.hide();
			_showCurrentNews();
		}
		function _onShow() {
			$base.fadeIn();
		}
		function _onHide() {
			$base.fadeOut( 300 );
		}
	}
	

	function onResize (){
		var w = $win.width();
		var sp = ( w <= 768 ) ? true : false;
		if ( isSP && !sp ) {
			// SP → PC
			openNav();
			setSearchPanel ( true );
		}
		if ( !isSP && sp ) {
			// PC → SP
			resetNav();
			setSearchPanel ( false );
		}
		isSP = sp;
	}

	function initHeader (){
		$(window).on('scroll', function() {
			if ( isSP ) {
				if ( $('header').hasClass( 'fixed') ) {
					$('header').removeClass( 'fixed slideInDown');
				}
				return true;
			}
			if ($(this).scrollTop() > HEADER_HEIGHT) {
				if ( !$('header').hasClass( 'fixed') ) {
					$('header').addClass( 'fixed slideInDown');
				}
			} else {
				if ( $('header').hasClass( 'fixed') ) {
					$('header').removeClass( 'fixed slideInDown');
				}
			}
		});
	}

	function initSearch (){
		var $search = $('.search');
		var $searchBtn = $('.icon-search');

		$searchBtn.on( 'click', function (){
			if ( !isSP ) {
				if ( !$search.hasClass('open') ) {
					$search.addClass('open');
				} else {
					$search.removeClass('open');
				}
			}
		});
	}

	function initAnchor (){

		$('a[href^=#]').click(function() {
			
			if ( $(this).hasClass('no-anchor') ) return true;

			// スクロールの速度
			var speed = 400; // ミリ秒
			// アンカーの値取得
			var href= $(this).attr("href");
			// 移動先を取得
			var target = $(href == "#" || href == "" ? 'html' : href);
			// 移動先を数値で取得
			var position = target.offset().top;
			// スムーススクロール
			$('body,html').animate({scrollTop:position}, speed, 'swing');
			return false;
		});
	}
	

	function initNav (){
		var str = location.pathname;
		var arr = str.split('/');
		var dir = arr[1];
		if ( dir == '' ) dir = 'top';
		$('.nav-' + dir).addClass('current');


		$('.header-nav-btn').on( 'click', function (){
			if ( $nav.hasClass('open') ) {
				closeNav();
			} else {
				openNav();
			}
		});
	}

	function openNav (){
		ga('send','event','top','click','SPメニュー＞オープン');

		$('.header-nav-btn').addClass('open');
		$nav.addClass('close open');
		/*
		current_scrollY = $( window ).scrollTop();
		$( 'body' ).css( {
			position: 'fixed',
			width: '100%',
			top: -1 * current_scrollY
		} );
		*/
	}

	function closeNav (){
		ga('send','event','top','click','SPメニュー＞クローズ');
		$('.header-nav-btn').removeClass('open');
		$nav.removeClass('open');

		//$( 'body' ).attr( { style: '' } );
		//$( 'html, body' ).prop( { scrollTop: current_scrollY } );
	}

	function resetNav (){
		$nav.removeClass('close');
		closeNav();
	}

	function setSearchPanel ( isPC ){
		$search = $('.search');
		if ( isPC ) {
			//$('.header-submenu').append( $search );
			$search.addClass('pc-rayout');
		} else {
			//$('.header-nav').append( $search );
			$search.removeClass('pc-rayout');
		}
	}

	function setCarouselBanner (){

		$('.slide-list-sp').slick({
			infinite: true,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay:true,
			arrows:false,
			speed:easeSpeed,
			autoplaySpeed:interval
		});
	}

	function partModal()
	{
		var $win = $( window );
		var modalTrigger;
		var modalWindow;
		var modalContent;
		var resize;

		var aspectW = 16;
		var aspectH = 9;
		
		_init();

		function _init()
		{
			//$win.on( MODAL_READY, _onContentReady );
			_onContentReady();
			$win.on( PLAY_COMPLETE, closeModalWrapper );
		}

		function onResize (){
			var w = $win.width();
			var h = $win.height();
			if ( w / h > aspectW / aspectH ) {
				// 横長
				var modalH = Math.round( h * 0.8 );
				var modalW = Math.round( modalH / aspectH * aspectW );
			} else {
				// 縦長
				var modalW = Math.round( w * 0.8 );
				var modalH = Math.round( modalW / aspectW * aspectH );
			}
			modalContent.css( { 'width': modalW, 'height': modalH } );
		}
		
		function _onContentReady()
		{
			
			/*------------------------------------*/
			modalTrigger = $('.cd-modal-trigger');
			modalWindow = $('.cd-modal');
			modalContent = $('.modal-content');
			resize = false;
			$win.on( 'hashchange', checkHash );
			if ( $('#yt_player_modal').length ) {
				$win.on( 'resize', onResize );
				onResize();
			}

			//close modal window
			modalWindow.on('click', '.modal-close', function(event){
				event.preventDefault();
				location.hash = 'modal_close';
			});
			checkHash ();
		}

		function closeModalWrapper (){
			location.hash = 'modal_close';
		}

		function closeModal (){
			$win.trigger( CLOSE_MODAL );
			modalWindow.removeClass('visible');
			if ( player_modal == null ) return;
			if ( typeof(player_modal.stopVideo) == "function" ) player_modal.stopVideo();
		}

		function checkHash () {
			var hash = location.hash;

			console.log('checkHash', hash, hash != '', player_modal);
			
			if (hash != '' && hash != '#modal_close') {
				modalTrigger.each(function(index, el) {
					var href = $(el).attr('href');
					var modalId = $(el).attr('data-target');
					var videoID = $(el).attr('data-target-id');
					if ( hash == href ) {
						openModalHash ( href, modalId, videoID );
						return false;
					}
				});
			} else{
				closeModal ();
			}
		}

		function openModalHash ( href, modID, dtID){

			if ( player_modal != null ) {
				player_modal.destroy();
			}
			
			var h = href.replace("#", "");
			var modalId = modID;
			var videoID = dtID;
			$win.trigger( new $.Event( OPEN_MODAL, { 'href': h } ) );
			modalWindow.filter(modalId).addClass('visible');
			
			if ( videoID ) YouTubeIframeAPIReady(videoID);
			//if ( typeof(player_modal.cueVideoById) == "function" ) player_modal.cueVideoById (videoID);
		}
	}


})( jQuery, window );




