/*----------------------------------------*/
/*  breadCrumb.js ( 2012/11/16 )
/*  http://tshinobu.com/lab/breadCrumbJs/
/*  readcrumb (topicpath) generator by javascript
/*----------------------------------------*/

breadCrumbJsData = function(){

	/*----------------------------------------*/
	/*  index.html をパンくずリスト生成に含めるかどうか定義します。
	/*  false … index.html を無視する
	/*  true … index.html を無視しない
	/*----------------------------------------*/
	this.indexMatch = false;

	/*----------------------------------------*/
	/*  パンくずリストから除外するパスを定義してください。
	/*  最後に「/」を付けないようにしてください。
	/*  除外しない場合 … ''
	/*  除外する場合    … '/lab/breadCrumbJs'
	/*----------------------------------------*/
	this.ignorePath = '';

	/*----------------------------------------*/
	/*  パンくずリストに表示する名前を定義してください。
	/*  （書式） "ディレクトリ名" : "表示名"
	/*----------------------------------------*/
	this.contentName = {
		"home" : "トップページ", // この行は残してください。
		"/message/":"メッセージ",
		"/product/" : "商品のこと",
		"/product/basic/" : "グランエックスのきほん",
		"/product/basic/jpg/" : "土鍋圧力IH炊飯ジャー（JPG-X100）",
		"/product/basic/kbd/" : "IHホームベーカリー（KBD-X100）",
		"/product/basic/kax/" : "スチームコンべクションオーブン（KAX-X130）",
		"/product/basic/acq/" : "コーヒーメーカー（ACQ-X020）",
		"/product/lineup/" : "ラインアップ",
		"/product/lineup/jpg/" : "土鍋圧力IH炊飯ジャー（JPG-X100）",
		"/product/lineup/jpg/switch.html" : "しあわせスイッチストーリー",
		"/product/lineup/jpg/style.html" : "Donabe Style",
		"/product/lineup/jpg/story.html" : "Donabe Story",
		"/product/lineup/kbd/" : "IHホームベーカリー（KBD-X100）",
		"/product/lineup/kbd/switch.html" : "しあわせスイッチストーリー みんなと一緒がうれしい編",
		"/product/lineup/kbd/switch02.html" : "しあわせスイッチストーリー しあわせの記憶編",
		"/product/lineup/kbd/style.html" : "Healthy Style",
		"/product/lineup/kax/" : "スチームコンべクションオーブン（KAX-X130）",
		"/product/lineup/kax/switch.html" : "しあわせスイッチストーリー",
		"/product/lineup/kax/style.html" : "Steam Style",
		"/product/lineup/acq/" : "コーヒーメーカー（ACQ-X020）",
		"/product/lineup/acq/switch.html" : "しあわせスイッチストーリー",
		"/product/lineup/acq/style.html" : "Press Style",
		"/product/lineup/acq/Interview.html" : "Special Interview",
		"/product/lineup/kbd/story.html" : "Healthy Story",

		"/food/" : "食材のこと",
		"/food/story/" : "nolink",
		"/food/story/koshihikari/" : "魚沼産コシヒカリ",
		"/food/story/navel/" : "瀬戸田ネーブル",
		"/food/story/vegetable/" : "有機野菜",
		"/food/story/pudding/" : "糀ぷりん",

		"/recipe/" : "レシピいろいろ",
		"/recipe/vol1/" : "#1 焼売",
		"/human/" : "あの人",
		"/human/50on.html" : "あの人（50音順）",
		"/human/vol1/" : "#1 あの人が「うん」と言う",
		"/human/vol2/" : "#1 グランエックス12ヶ月",
		"/human/vol3/" : "#1 きほんのいいもの",
		"/human/vol4/" : "#1 わたしの台所",
		"/human/vol5/" : "#2 グランエックス12ヶ月",
		"/human/vol6/" : "#2 きほんのいいもの",
		"/human/vol7/" : "#2 わたしの台所",
		"/human/vol8/" : "#1 あの人に聞きました",
		"/human/vol9/" : "#1 わたしの台所",
		"/human/vol10/" : "#3 わたしの台所",
		"/human/vol11/" : "#1 きほんのいいもの",
		"/human/vol12/" : "#2 わたしの台所",
		"/human/vol13/" : "#3 きほんのいいもの",
		"/human/vol14/" : "#3 グランエックス12ヶ月",
		"/human/vol15/" : "#2 あの人に聞きました",
		"/human/vol16/" : "#2 きほんのいいもの",
		"/human/vol17/" : "#3 わたしの台所",

		"/movie/" : "nolink",
		"/movie/story.html" : "動画",
		"/movie/basic.html" : "動画",
		"/movie/tvcm.html" : "動画",
		"/special/" : "スペシャル",
		"/special/cp1/" : "グランエックス早期購入キャンペーン",
		"/special/nozomi/" : "希のとっておき",
		"/special/nozomi-01/" : "希のとっておき",
		"/special/nozomi-02/" : "希のとっておき",
		"/special/nozomi-03/" : "希のとっておき",
		"/special/nozomi-04/" : "希のとっておき",
		"/special/nozomi-04/" : "希のとっておき",

		"/search/" : "検索結果",
		"/tag_search/" : "タグ検索結果",
		"/sitemap/" : "サイトマップ",
		"" : "" //この行は残してください。
	};
}
var breadCrumbJsData = new breadCrumbJsData();

function breadCrumbJs(){
	/*----------------------------------------*/
	/*  以下条件分岐 / 表示処理部分
	/*----------------------------------------*/
	var URL = window.location.pathname.replace(breadCrumbJsData.ignorePath, '');
	var thisURL = URL.match(/(.*?)\//g);
	var fileName =  window.location.pathname.match(/([^¥/]+?)$/);
	
	if ( fileName && (!( fileName[0].match("index") && !breadCrumbJsData.indexMatch )) ){ thisURL.push( fileName[0] ); }
	var drw = document.getElementById("breadCrumb");
	var rootingPath = "";
	if ( drw.tagName == "P" | drw.tagName == "DIV" ){
		for( i=0; i<thisURL.length; i++){
			rootingPath += thisURL[i];
			var nm;
			if ( i == 0 ){
				nm = funcIndexSearch('home');
				if (nm == 'nolink') continue;
				drw.innerHTML = '<a href="'+breadCrumbJsData.ignorePath+'/">' + nm + '</a>';
			} else if ( i == thisURL.length - 1 ){
				nm = funcIndexSearch(rootingPath);
				if (nm == 'nolink') continue;
				drw.innerHTML += ' &gt; <strong>' + nm + '</strong>';
			} else {
				nm = funcIndexSearch(rootingPath);
				if (nm == 'nolink') continue;
				drw.innerHTML += ' &gt; <a href="'+breadCrumbJsData.ignorePath+''+rootingPath+'">' + nm + '</a>';
			}
		}
	}
	if ( drw.tagName == "UL" | drw.tagName == "OL" ){
		for( i=0; i<thisURL.length; i++){
			rootingPath += thisURL[i];
			var nm;
			if ( i == 0 ){
				nm = funcIndexSearch('home');
				if (nm == 'nolink') continue;
				drw.innerHTML = '<li><a href="'+breadCrumbJsData.ignorePath+'/">' + nm + '</a></li>';
			} else if ( i == thisURL.length - 1 ){
				nm = funcIndexSearch(rootingPath);
				if (nm == 'nolink') continue;
				drw.innerHTML += ' <li class="active"><strong>' + nm + '</strong></li>';
			} else {
				nm = funcIndexSearch(rootingPath);
				if (nm == 'nolink') continue;
				drw.innerHTML += ' <li><a href="'+breadCrumbJsData.ignorePath+''+rootingPath+'">' + nm + '</a></li>';
			}
		}
	}
}
function funcIndexSearch(){
	/*----------------------------------------*/
	/*  パンくずリスト生成検索処理
	/*----------------------------------------*/
	keyword = breadCrumbJsData.contentName[arguments[0]];
	if(keyword == undefined){
		return arguments[0].match(/(.*?)\//g).pop().replace("/","");
	} else{
		return keyword;
	}
}

if(window.addEventListener) {
	window.addEventListener("load", breadCrumbJs, false);
}
else if(window.attachEvent) {
	window.attachEvent("onload", breadCrumbJs);
}