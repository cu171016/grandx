<?php
/**
 * Template part for displaying use posts
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
$postid = get_the_ID();
?>
        <section id="post<?php echo $postid; ?>">
          <?php the_title( '<h2 class="p-list-title sp">', '</h2>' ); ?>
          <div class="c-media">
            <?php if ( '' !== get_the_post_thumbnail() ) { 
                echo '<img src="'.get_the_post_thumbnail_url( $postid , 'full' ).'" alt="'.get_post_meta( get_post_thumbnail_id( $postid ), '_wp_attachment_image_alt', true ).'">';
            } ?>
          </div>
          <div class="p-text">
            <?php the_title( '<h2 class="p-list-title pc">', '</h2>' ); ?>
            <p><?php echo get_post_time('Y.m.d'); ?></p>
            <p>
            <?php
              /* translators: %s: Name of current post */
              the_content( sprintf(
                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
                get_the_title()
              ) );
            ?>
            </p>
          </div>
        </section>
