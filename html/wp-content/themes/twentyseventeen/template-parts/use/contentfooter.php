        <section class="p-mono-list">
        <h3>商品で見る</h3>

        <ul>
          <?php
            $terms = get_terms('use_product');
            foreach ( $terms as $term ) {
              if( $term->slug == "jpg") {
          ?>
          <li>
            <a href="<?php echo get_term_link( $term ); ?>">
              <div class="thumb">
                <?php
                  $imgid = get_field('productimg',$term);
                  $image = wp_get_attachment_image_src($imgid, 'full');
                  $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
                ?>
                <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>">
              </div>
              <p class="product-name">
                <?php echo  $term->name; ?><br>
                <?php the_field('productmpn',$term); ?>
              </p>
            </a>
          </li>
          <?php 
              }
            }
          ?>
          <?php 
            foreach ( $terms as $term ) {
              if( $term->slug == "kbd") {
          ?>
          <li>
            <a href="<?php echo get_term_link( $term ); ?>">
              <div class="thumb">
                <?php
                  $imgid = get_field('productimg',$term);
                  $image = wp_get_attachment_image_src($imgid, 'full');
                  $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
                ?>
                <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>">
              </div>
              <p class="product-name">
                <?php echo  $term->name; ?><br>
                <?php the_field('productmpn',$term); ?>
              </p>
            </a>
          </li>
          <?php 
              }
            }
          ?>
          <?php 
            foreach ( $terms as $term ) {
              if( $term->slug == "kax") {
          ?>
          <li>
            <a href="<?php echo get_term_link( $term ); ?>">
              <div class="thumb">
                <?php
                  $imgid = get_field('productimg',$term);
                  $image = wp_get_attachment_image_src($imgid, 'full');
                  $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
                ?>
                <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>">
              </div>
              <p class="product-name">
                <?php echo  $term->name; ?><br>
                <?php the_field('productmpn',$term); ?>
              </p>
            </a>
          </li>
          <?php 
              }
            }
          ?>
          <?php 
            foreach ( $terms as $term ) {
              if( $term->slug == "acq") {
          ?>
          <li>
            <a href="<?php echo get_term_link( $term ); ?>">
              <div class="thumb">
                <?php
                  $imgid = get_field('productimg',$term);
                  $image = wp_get_attachment_image_src($imgid, 'full');
                  $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
                ?>
                <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>">
              </div>
              <p class="product-name">
                <?php echo  $term->name; ?><br>
                <?php the_field('productmpn',$term); ?>
              </p>
            </a>
          </li>
          <?php 
              }
            }
          ?>
        </ul>
        </section>

        <section class="p-hito-list">
        <h3>人で見る</h3>
        <ul>
          <?php
            $terms = get_terms('use_person');
            foreach ( $terms as $term ) {
          ?>
          <li>
            <a href="<?php echo get_term_link( $term ); ?>">
              <?php
                $imgid = get_field('faceimg',$term);
                $image = wp_get_attachment_image_src($imgid, 'full');
                $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
              ?>
              <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>"><br>
              <?php echo  $term->name; ?><br>
              <span>（ <?php the_field('kana',$term); ?>）</span>
            </a>
          </li>
          <?php } ?>
        </ul>
        </section>

        <section class="contents-section">
          <ul class="two-column">
            <li class="hover-animation">
              <a href="<?php echo home_url(); ?>/product/basic/">
                <img src="/assets/images/product/use/content-thumb01.jpg" alt="グランエックスのきほん" class="pc">
                <img src="/assets/images/product/use/content-thumb01-sp.jpg" alt="グランエックスのきほん" class="sp">
                <div class="label">
                  <div class="inner">
                    <p class="font-ica item-name">グランエックスのきほん</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="hover-animation">
              <a href="<?php echo home_url(); ?>/product/lineup/">
                <img src="/assets/images/product/use/content-thumb02.jpg" alt="ラインアップ" class="pc">
                <img src="/assets/images/product/use/content-thumb02-sp.jpg" alt="ラインアップ" class="sp">
                <div class="label">
                  <div class="inner">
                    <p class="font-ica item-name">ラインアップ</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </section>
