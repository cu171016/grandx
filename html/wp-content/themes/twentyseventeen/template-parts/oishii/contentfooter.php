        <section class="p-season-list">
        <h3>季節で見る</h3>

        <ul>
          <?php
            $terms = get_terms('oishii_season',array( 'orderby' => $terms->id));
            foreach ( $terms as $term ) {
          ?>
          <li>
            <a href="<?php echo get_term_link( $term ); ?>">
              <div class="thumb">
                <?php
                  $imgid = get_field('season_img',$term);
                  $image = wp_get_attachment_image_src($imgid, 'full');
                  $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
                ?>
                <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>">
              </div>
            </a>
          </li>
          <?php 
            }
          ?>
        </ul>
        </section>

        <section class="p-type-list">
        <h3>種類で見る</h3>
        <ul>
          <?php
            $terms = get_terms('oishii_foods', array( 'orderby' => $terms->id));
            foreach ( $terms as $term ) {
          ?>
          <li>
            <a href="<?php echo get_term_link( $term ); ?>">
              <?php
                $imgid = get_field('food_img',$term);
                $image = wp_get_attachment_image_src($imgid, 'full');
                $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
              ?>
              <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>"><br>
              <?php echo $term->name; ?>
            </a>
          </li>
          <?php } ?>
        </ul>
        </section>

        <section class="contents-section">
          <ul class="two-column">
            <li class="hover-animation">
              <a href="<?php echo home_url(); ?>/product/basic/">
                <img src="/assets/images/product/use/content-thumb01.jpg" alt="グランエックスのきほん" class="pc">
                <img src="/assets/images/product/use/content-thumb01-sp.jpg" alt="グランエックスのきほん" class="sp">
                <div class="label">
                  <div class="inner">
                    <p class="font-ica item-name">グランエックスのきほん</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="hover-animation">
              <a href="<?php echo home_url(); ?>/product/lineup/">
                <img src="/assets/images/product/use/content-thumb02.jpg" alt="ラインアップ" class="pc">
                <img src="/assets/images/product/use/content-thumb02-sp.jpg" alt="ラインアップ" class="sp">
                <div class="label">
                  <div class="inner">
                    <p class="font-ica item-name">ラインアップ</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </section>

