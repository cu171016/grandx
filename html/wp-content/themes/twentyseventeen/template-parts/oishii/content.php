<?php
/**
 * Template part for displaying oishii posts
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
$postid = get_the_ID();
?>

<script type="text/javascript">
$(function(){
  	$('#breadCrumb li').eq(1).remove();
});
</script>
        <section id="post<?php echo $postid; ?>">
          <?php the_title( '<h2 class="p-list-title sp">', '</h2>' ); ?>
          <div class="c-media">
            <?php if ( '' !== get_the_post_thumbnail() ) { 
                echo '<img src="'.get_the_post_thumbnail_url( $postid , 'full' ).'" alt="'.get_post_meta( get_post_thumbnail_id( $postid ), '_wp_attachment_image_alt', true ).'">';
            } ?>
          </div>
          <div class="p-text">
            <?php the_title( '<h2 class="p-list-title pc">', '</h2>' ); ?>
            <p>
            <?php
              /* translators: %s: Name of current post */
              the_content( sprintf(
                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
                get_the_title()
              ) );
              if( $post->linktext ) {
            ?>
            <br><br>
            <a href="<?php echo $post->linkurl; ?>"<?php echo $post->linktgt[0] === 'checked' ? ' target="_blank"' : ''; ?>><?php echo $post->linktext; ?></a>
            <?php
              }
              if( $post->linknotice ) {
               echo '<br>' . $post->linknotice;
               }
             ?>
            </p>
          </div>
        </section>
