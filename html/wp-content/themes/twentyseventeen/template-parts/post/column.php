    <?php /* コラム */ ?>
    <?php 
    $imgsid = get_field('column_img');
    if(get_field('column_title') || get_field('column_subtitle') || $imgsid || get_field('column_text') || get_field('column_note') ): ?>
              <div class="p-hmKoborebanashi wow fadeIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">
                <?php if ($post->column_title) : ?>
                <h3 class="p-hmKoborebanashi__title"><?php echo nl2br($post->column_title); ?></h3>
                <?php endif; ?>
                <?php if ($post->column_subtitle) : ?>
                <p class="p-hmKoborebanashi__catch"><?php echo nl2br($post->column_subtitle); ?></p>
                <?php endif; ?>
                
                <div class="p-flex">
                  <?php if ($imgsid) : ?>
                  <div class="p-flex__elem p-hmKoborebanashi__photo p-flexMargin">
                    <?php 
                    if ($imgsid) :
                      $imgs= wp_get_attachment_image_src($imgsid, 'full');
                      $alts = get_post_meta($imgsid, '_wp_attachment_image_alt', true);
                    ?>
                    <img class="c-media__image" src="<?php echo $imgs[0]; ?>" alt="<?php echo $alts; ?>">
                    <?php endif; ?>
                  </div>
                  <?php endif; ?>
                  <?php if ($post->column_text || $post->column_note) : ?>
                  <div class="p-flex__elem p-flexMargin">
                    <?php if ($post->column_text) : ?>
                    <p class="p-hm-p"><?php echo nl2br($post->column_text); ?></p>
                    <?php endif; ?>
                    <?php if ($post->column_note) : ?>
                    <p class="p-hmKoborebanashi__notice"><?php echo nl2br($post->column_note); ?></p>
                    <?php endif; ?>
                  </div>
                  <?php endif; ?>
                </div>
              </div>
    <?php endif; ?>
