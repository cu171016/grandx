<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX,グランエックス,タイガー魔法瓶,土鍋圧力炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー,レシピ,レシピいろいろ">
<meta name="description" content="タイガーGRAND X(グランエックス)シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[レシピいろいろ]">
<link rel="shortcut icon" type="image/x-icon" href="/assets/images/common/favicon.ico">

<meta property="og:url" content="https://www.grandx.jp/recipe/">
<meta property="og:type" content="website">
<meta property="og:title" content="レシピいろいろ｜GX GRANDX クラブ｜タイガー魔法瓶">
<meta property="og:description" content="タイガーGRAND X(グランエックス)シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[レシピいろいろ]">
<meta property="og:image" content="https://www.grandx.jp/assets/images/common/ogp.png">
<meta property="og:site_name" content="レシピいろいろ｜GX GRANDX クラブ｜タイガー魔法瓶">

<title>レシピいろいろ｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="/assets/css/normalize.css">
<link rel="stylesheet" href="/assets/css/common.css">
<link rel="stylesheet" href="/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5b8a7e780eec4098b0320d8bac1e02ec" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="/assets/js/jquery-1.11.0.min.js"></script>
<script src="/assets/js/breadCrumb.js"></script>
<script src="/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li class="active">レシピいろいろ</li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <main role="main" class="main-content">
      <div class="content-inner detail category-index child">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->

        <h1 class="h1-contents-title">レシピいろいろ</h1>

        <section class="contents-section">
          <ul class="two-column">
<?php // レシピ一覧取得
$args = array(
  'category_name' => 'recipe',
  'posts_per_page' => -1,
  'orderby' => 'post-date',
);
$the_query = new WP_Query($args);
if ( $the_query->have_posts() ) :
  while ( $the_query->have_posts() ) : $the_query->the_post();
?>
            <li class="hover-animation">
              <a href="<?php the_permalink(); ?>">
<?php 
$image = get_field('category_image_pc');
if($image):
?>
                <img class="bg pc" src="<?php echo $image; ?>" alt="">
<?php endif; ?>
<?php 
$image = get_field('category_image_sp');
if($image):
?>
                <img class="bg sp" src="<?php echo $image; ?>" alt="">
<?php endif; ?>
                <div class="label orange">
                  <div class="inner">
                    <p class="item-name-large"><?php the_title(); ?></p>
                    <p class="item-name-middle"><span class="item-name-min">#</span><?php the_field('number'); ?> <?php the_field('menu'); ?></p>
                  </div>
                </div>
              </a>
            </li>
<?php
  endwhile;
endif;
?>
          </ul>

          <a class="p-rcp-idxBanner" href="/special/nozomi-01/"><img class="pc" src="/assets/images/special/bnr-cp2.jpg" alt=""><img class="sp" src="/assets/images/special/bnr-cp2-sp.jpg" alt=""></a>
          
        </section>
        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->
      </div>
    </main>
  </div>

    <footer class="footer">
    <aside class="footer-content">
      <div class="four-column flex">
        <div class="column-block footer-msg">
          <p class="column-inner">食卓に新たな価値を提案するブランド。<br>
〜五感に響きわたる、おいしさと上質感を。〜</p>
        </div>

        <script src="/assets/js/instafeed.min.js"></script>
        <div class="column-block footer-instagram">
          <div class="column-inner">
            
            <p class="footer-sns-title">公式SNSアカウント</p>

            <div class="block-instagram">
              <p class="logo-instagram"><img class="pc" src="/assets/images/common/logo-instagram.png" alt="Instagram"><img class="sp" src="/assets/images/common/logo-instagram-sp.png" alt="Instagram"></p>
              
              <ul class="instagram-thumb-list" id="insta_list">
               
              </ul>
            </div>
          </div>
        </div>

        <div class="column-block footer-facebook">
          <div class="column-inner">
            

            <!-- Facebook -->
            <div class="block-facebook">
              <div class="fb-page" data-href="https://www.facebook.com/g.x.tiger/?fref=ts" data-tabs="timeline" data-width="' + w + '" data-height="' + h + '" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/g.x.tiger/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/g.x.tiger/?fref=ts">GRAND X（グランエックス）</a></blockquote></div>
              
            </div>
            <a class="btn-facebook" href="https://www.facebook.com/g.x.tiger/?fref=ts" target="_blank" onclick="ga('send','event','top','click','フッター＞フェイスブック');"><img class="pc" src="/assets/images/common/btn-facebook.png" alt="Facebook"><img class="sp" src="/assets/images/common/btn-facebook-sp.png" alt="Facebook"></a>
          </div>
        </div>

        <div class="column-block footer-link">
          <ul class="column-inner">
            <li class="footer-link-kenko"><a href="https://oishi-kenko.com/" target="_blank" onclick="ga('send','event','top','click','フッター＞おいしい健康ロゴ');"><img class="pc" src="/assets/images/common/footer-link-kenko.png" alt="おいしい健康"><img class="sp" src="/assets/images/common/footer-link-kenko-sp.png" alt="おいしい健康"></a></li>
            <li class="footer-link-kihon"><a href="https://kurashi-no-kihon.com/" target="_blank" onclick="ga('send','event','top','click','フッター＞くらしのきほんロゴ');"><img class="pc" src="/assets/images/common/footer-link-kihon.png" alt="くらしのきほん"><img class="sp" src="/assets/images/common/footer-link-kihon-sp.png" alt="くらしのきほん"></a></li>
            <li class="footer-link-balloon"><a href="https://balloonfrom.com/" target="_blank" onclick="ga('send','event','top','click','フッター＞BALLONロゴ');"><img class="pc" src="/assets/images/common/footer-link-balloon.png" alt="Balloon"><img class="sp" src="/assets/images/common/footer-link-balloon-sp.png" alt="Balloon"></a></li>
          </ul>
        </div>
      </div>

      <div class="footer-link-tiger"><a href="https://www.tiger.jp/" target="_blank" onclick="ga('send','event','top','click','フッター＞タイガーロゴ');"><img class="pc" src="/assets/images/common/footer-link-tiger.png" alt="TIGER"><img class="sp" src="/assets/images/common/footer-link-tiger-sp.png" alt="TIGER"></a></div>
    </aside>

    <a class="btn-backtop" href="#"><img class="pc" src="/assets/images/common/btn-backtop.png" alt="Balloon"><img class="sp" src="/assets/images/common/btn-backtop-sp.png" alt="Balloon"></a>

    <p class="copyright">Copyright &copy; Tiger Corporation., All Rights Reserved.</p>

  </footer><!-- // end of footer -->
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>