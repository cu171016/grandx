<!--
/*
Template Name: あの人 B おはぎ
Template Post Type: post, page
*/
-->
<!DOCTYPE HTML>
<html>
<?php
// article area start
while ( have_posts() ) : the_post();
    $currentId = get_the_ID();
    $who = get_field('who');
    $category = get_the_category();
    $cat_name = $category[0]->cat_name;
    $cat_slug = $category[0]->category_nicename;
?>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="<?php the_field('meta_keywords'); ?>">
<meta name="description" content="<?php the_field('meta_description'); ?>">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="<?php echo get_permalink(get_the_ID()); ?>">
<meta property="og:type" content="website">
<meta property="og:title" content="<?php the_field('ogp_title'); ?>">
<meta property="og:description" content="<?php the_field('ogp_description'); ?>">
<meta property="og:image" content="<?php the_field('ogp_image'); ?>">
<meta property="og:site_name" content="<?php the_field('ogp_site_name'); ?>">

<title><?php the_field('who'); ?>さん｜<?php echo $cat_name; ?>｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5b8a7e780eec4098b0320d8bac1e02ec" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li><a href="/<?php echo $cat_slug; ?>/"><?php echo $cat_name; ?></a></li>
        <li class="active">#<?php the_field('number'); ?> <?php the_title(); ?></li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <style>
<?php if( $post->main_image_pc ) : ?>
      .cover {
        background: url("<?php the_field('main_image_pc'); ?>") center center no-repeat;
        background-size: cover; }
<?php endif; ?>
<?php if( $post->main_image_sp ) : ?>
      @media only screen and (max-width: 768px) {
        .cover {
          background: url("<?php the_field('main_image_sp'); ?>") center center no-repeat;
          background-size: cover; }
      }
<?php endif; ?>
    </style>

    <div class="cover"></div>

    <main role="main" class="main-content">
      <div class="main-content__inner">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->

        <section class="contents-section contents-section--section1">
          <h1 class="p-h2-covertitle">
            <span class="p-h2-covertitle__sub-cpoy--hmv2"><?php the_field('publish'); ?></span>
            <span class="p-h2-covertitle__main-copy"><?php the_field('menu'); ?></span>
          </h1>
          <h2 class="p-h2cache p-h2cache--hmv2"><?php the_title(); ?><br><span class="p-h2cache--hmv2__name"><?php the_field('who');?><span>さん</span></span></h2>

          <div class="p-flex">
            <div class="p-flex__elem p-flexMargin">
<?php if( get_field('gray_text1') ): ?>
              <p class="cache-g cache-g--p1 wow fadeIn" data-wow-delay="0.5s"><?php the_field('gray_text1'); ?></p>
<?php endif; ?>

<?php if( get_field('black_text1') ): ?>
              <p class="p-hm-p wow fadeIn font-ica" data-wow-delay="0.5s"><?php the_field('black_text1'); ?></p>
<?php endif; ?>
            </div>

            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media">
<?php 
$image = get_field('image1_sp');
if($image):
?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image1_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
              </div>

<?php if( get_field('image1_description') ): ?>
              <div class="figue__caption" class="c-figcap--p1"><?php the_field('image1_description'); ?></div>
<?php endif; ?>

<?php if( get_field('black_text2') ): ?>
              <p class="p-hm-p wow fadeIn font-ica" data-wow-delay="0.5s"><?php the_field('black_text2'); ?></p>
<?php endif; ?>

            </div>
          </div>


<?php if( get_field('headline1') || get_field('black_text3') || get_field('image2_pc') || get_field('image2_sp') || get_field('image2_description')): ?>
          <div class="p-flex p-flex--hm2">
            <div class="p-flex__elem p-flexMargin">
    <?php if( get_field('headline1') ): ?>
              <h3 class="p-hm-h3 wow fadeIn" data-wow-delay="0.5s"><?php the_field('headline1'); ?></h3>
    <?php endif; ?>

    <?php if( get_field('black_text3') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text3'); ?></p>
    <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media wow fadeIn" data-wow-delay="0.5s">
    <?php
    $image = get_field('image2_sp');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image2_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if( get_field('image2_description') ): ?>
              <div class="figue__caption"><?php the_field('image2_description'); ?></div>
    <?php endif; ?>
            </div>
          </div>
<?php endif; ?>



<?php if( get_field('black_text4') || get_field('image3_pc') || get_field('image3_sp') || get_field('image3_description')): ?>
          <div class="p-flex p-flex--hm2">
            <div class="p-flex__elem p-flexMargin">
    <?php if( get_field('black_text4') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text4'); ?></p>
    <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media wow fadeIn" data-wow-delay="0.5s">
    <?php 
    $image = get_field('image3_sp');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image3_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>

              </div>
    <?php if( get_field('image3_description') ): ?>
              <div class="figue__caption"><?php the_field('image3_description'); ?></div>
    <?php endif; ?>
            </div>
          </div>
<?php endif; ?>


<?php if( get_field('headline1_2') || get_field('black_text4_2') || get_field('image3_2_pc') || get_field('image3_2_sp') || get_field('image3_2_description')): ?>
          <div class="p-flex p-flex--reverse">
            <div class="p-flex__elem p-flexMargin">
    <?php if( get_field('headline1_2') ): ?>
              <h3 class="p-hm-h3 wow fadeIn" data-wow-delay="0.5s"><?php the_field('headline1_2'); ?></h3>
    <?php endif; ?>

    <?php if( get_field('black_text4_2') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text4_2'); ?></p>
    <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media wow fadeIn" data-wow-delay="0.5s">
    <?php 
    $image = get_field('image3_2_sp');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image3_2_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if( get_field('image3_2_description') ): ?>
              <div class="figue__caption"><?php the_field('image3_2_description'); ?></div>
    <?php endif; ?>
            </div>
        </div>
<?php endif; ?>




<?php if( get_field('headline1_3') || get_field('black_text4_3') || get_field('image3_3_pc') || get_field('image3_3_sp') || get_field('image3_3_description')): ?>
          <div class="p-flex p-flex--reverse">
            <div class="p-flex__elem p-flexMargin">
    <?php if( get_field('headline1_3') ): ?>
              <h3 class="p-hm-h3 wow fadeIn" data-wow-delay="0.5s"><?php the_field('headline1_3'); ?></h3>
    <?php endif; ?>

    <?php if( get_field('black_text4_3') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text4_3'); ?></p>
    <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media wow fadeIn" data-wow-delay="0.5s">
    <?php 
    $image = get_field('image3_3_sp');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image3_3_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if( get_field('image3_3_description') ): ?>
              <div class="figue__caption"><?php the_field('image3_3_description'); ?></div>
    <?php endif; ?>
            </div>
        </div>
<?php endif; ?>




<?php if( get_field('headline1_4') || get_field('black_text4_4') || get_field('image3_4_pc') || get_field('image3_4_sp') || get_field('image3_4_description')): ?>
          <div class="p-flex p-flex--reverse">
            <div class="p-flex__elem p-flexMargin">
    <?php if( get_field('headline1_4') ): ?>
              <h3 class="p-hm-h3 wow fadeIn" data-wow-delay="0.5s"><?php the_field('headline1_4'); ?></h3>
    <?php endif; ?>

    <?php if( get_field('black_text4_4') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text4_4'); ?></p>
    <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media wow fadeIn" data-wow-delay="0.5s">
    <?php 
    $image = get_field('image3_4_sp');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image3_4_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if( get_field('image3_4_description') ): ?>
              <div class="figue__caption"><?php the_field('image3_4_description'); ?></div>
    <?php endif; ?>
            </div>
        </div>
<?php endif; ?>


        </section>






        <section class="p-recipe p-recipe--padding">
<?php if( get_field('headline2') ): ?>
          <h3 class="p-hm-rcpTitle"><?php the_field('headline2'); ?></h3>
<?php endif; ?>
          
          <div class="p-recipe__inner border-dot">
            <div class="p-grid--rcp wow fadeIn" data-wow-delay="0.5s">
              <div class="p-grid--rcp__grd1">
                <p class="p-recipe__label p-recipe__label_z">［材料］</p>
                <dl class="c-dl-table">
<?php if( get_field('material_headline') ): ?>
                  <dt class="lbl"><?php the_field('material_headline'); ?></dt>
<?php endif; ?>

<?php
$material = get_field('material');
if($material):
    $list = explode("\n", $material);
    foreach ($list as $value) {
        $parts = explode("：", $value);
        printf("<dt>%s</dt><dd>%s</dd>", $parts[0], $parts[1]);
    }
?>
<?php endif; ?>
                </dl>

<?php 
$image = get_field('recipe_image_sp');
if($image):
?>
                <div class="p-rcp-vol2__photo c-media"><img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->recipe_image_sp, '_wp_attachment_image_alt', true ); ?>"></div>
<?php endif; ?>

              </div>

              <div class="p-grid--rcp__grd2">
                <p class="p-recipe__label">［作り方］</p>

                <ol class="p-process">
<?php
$howToMake = get_field('how_to_make');
if($howToMake):
    $list = explode("\n", $howToMake);
    foreach ($list as $value) {
        printf("<li>%s</li>", $value);
    }
?>
<?php endif; ?>
                </ol>

<?php if( get_field('how_to_make_note') ): ?>
                <p class="p-notice"><?php the_field('how_to_make_note'); ?></p>
<?php endif; ?>
              </div>
            </div>
          </div>
        </section>

<?php if( get_field('headline3') || get_field('black_text5') || get_field('image4_pc') || get_field('image4_sp') || get_field('image4_description')
       || get_field('headline3') || get_field('black_text5') || get_field('image4_pc') || get_field('image4_sp') || get_field('image4_description')): ?>


        <section class="contents-section contents-section--section1">
    <?php if( get_field('headline3') || get_field('black_text5') || get_field('image4_pc') || get_field('image4_sp') || get_field('image4_description')): ?>
          <div class="p-flex">
            <div class="p-flex__elem p-flexMargin">
        <?php if( get_field('headline3') ): ?>
              <h3 class="p-hm-h3 wow fadeIn" data-wow-delay="0.5s"><?php the_field('headline3'); ?></h3>
        <?php endif; ?>

        <?php if( get_field('black_text5') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text5'); ?></p>
        <?php endif; ?>

            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media wow fadeIn" data-wow-delay="0.5s">
        <?php 
        $image = get_field('image4_sp');
        if($image):
        ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image4_sp, '_wp_attachment_image_alt', true ); ?>">
        <?php endif; ?>
              </div>
        <?php if( get_field('image4_description') ): ?>
              <div class="figue__caption"><?php the_field('image4_description'); ?></div>
        <?php endif; ?>
            </div>
          </div>
    <?php endif; ?>

    <?php if( get_field('headline3') || get_field('black_text5') || get_field('image4_pc') || get_field('image4_sp') || get_field('image4_description') || get_field('credit')): ?>
          <div class="p-flex p-flex--hm2">
            <div class="p-flex__elem p-flexMargin">
        <?php if( get_field('black_text6') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text6'); ?></p>
        <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media wow fadeIn" data-wow-delay="0.5s">
        <?php 
        $image = get_field('image5_sp');
        if($image):
        ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image5_sp, '_wp_attachment_image_alt', true ); ?>">
        <?php endif; ?>
              </div>

        <?php if( get_field('image5_description') ): ?>
              <div class="figue__caption"><?php the_field('image5_description'); ?></div>
        <?php endif; ?>

        <?php if( get_field('credit') ): ?>
              <p class="p-credit wow fadeIn" data-wow-delay="0.5s"><?php the_field('credit'); ?></p>
        <?php endif; ?>
            </div>
          </div>
    <?php endif; ?>

        </section>
<?php elseif( get_field('credit') ): ?>
        <p class="p-credit p-credit--vol5 wow fadeIn" data-wow-delay="0.5s"><?php the_field('credit'); ?></p>
<?php endif; ?>
        
        <?php get_template_part('profile'); ?>
        
        <section class="contents-section">

          <?php include locate_template('pager.php'); ?>

　        <?php get_template_part('nav-sns'); ?>
        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->
        </section>
      </div>


    </main>
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
<?php
//article area end
endwhile;
?>

</html>