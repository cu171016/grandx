<!--
/*
Template Name: あの人 G フリーテンプレート
Template Post Type: post, page
*/
-->
<!DOCTYPE HTML>
<html>
<?php
// article area start
while ( have_posts() ) : the_post();
    $currentId = get_the_ID();
    $who = get_field('who');
    $category = get_the_category();
    $cat_name = $category[0]->cat_name;
    $cat_slug = $category[0]->category_nicename;
?>
<head>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="<?php the_field('meta_keywords'); ?>">
<meta name="description" content="<?php the_field('meta_description'); ?>">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="<?php echo get_permalink(get_the_ID()); ?>">
<meta property="og:type" content="website">
<meta property="og:title" content="<?php the_field('ogp_title'); ?>">
<meta property="og:description" content="<?php the_field('ogp_description'); ?>">
<meta property="og:image" content="<?php the_field('ogp_image'); ?>">
<meta property="og:site_name" content="<?php the_field('ogp_site_name'); ?>">

<title><?php the_field('who'); ?>さん｜<?php echo $cat_name; ?>｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5b8a7e780eec4098b0320d8bac1e02ec" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li><a href="/message">メッセージ</a></li>
        <li class="active"><?php if(get_field('number')): ?>#<?php the_field('number'); ?> <?php endif; ?><?php the_title(); ?></li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <style>
<?php if( $post->main_image_pc ) : ?>
      .cover {
        background: url("<?php the_field('main_image_pc'); ?>") center center no-repeat;
        background-size: cover; }
<?php endif; ?>
<?php if( $post->main_image_sp ) : ?>
      @media only screen and (max-width: 768px) {
        .cover {
          background: url("<?php the_field('main_image_sp'); ?>") center center no-repeat;
          background-size: cover; }
      }
<?php endif; ?>
    </style>

    <div class="cover"></div>

    <main role="main" class="main-content">
      <div class="main-content__inner">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->

        <section class="contents-section contents-section--section1">

          <h1 class="p-h2cache <?php the_field('title_position_pc'); ?> <?php the_field('title_position_sp'); ?>"><?php the_title(); ?><br><?php if(get_field('who')): ?><span class="p-h2cache--hmv3__name"><?php the_field('who'); ?><span>さん</span></span><?php endif; ?></h1>

<?php if(get_field('number') || get_field('headline1')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <div class="p-flex__elem p-flexMargin">
              <h3 class="p-hm-h3--cache font-ica"><span>#<?php the_field('number'); ?></span><br><?php the_field('headline1'); ?></h3>
            </div>
            <div class="p-flex__elem p-flexMargin">              
            </div>
          </div>
<?php endif; ?>

<?php if(get_field('black_text1') || get_field('image1_pc') || get_field('image1_sp')|| get_field('image1_description')): ?>
          <div class="p-flex p-flex--reverse wow fadeIn" data-wow-delay="0.5s">
            <div class="p-flex__elem p-flexMargin p-flex__elem p-flexMargin--top0">
              <p class="p-hm-p font-ica"><?php the_field('black_text1'); ?></p>
            </div>
            <div class="p-flex__elem p-flexMargin">
              <div class="c-media p-hm-thumb--large p-hm-thumb--large-left">
    <?php 
    $image = get_field('image1_pc');
    if($image):
    ?>
                <img class="c-media__image pc" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image1_pc, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>

    <?php 
    $image = get_field('image1_sp');
    if($image):
    ?>
                <img class="c-media__image sp" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image1_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
              <div class="figue__caption"><?php the_field('image1_description'); ?></div>
            </div>
          </div>
<?php endif; ?>


<?php if(get_field('headline2') || get_field('black_text2')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <h3 class="p-hm-h3"><?php the_field('headline2'); ?></h3>
            <p class="p-hm-p"><?php the_field('black_text2'); ?></p>
          </div>
<?php endif; ?>


<?php if(get_field('headline3') || get_field('black_text3') || get_field('image2_pc') || get_field('image2_sp')|| get_field('image2_description')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <div class="p-flex__elem p-flexMargin">
              <h3 class="p-hm-h3"><?php the_field('headline3'); ?></h3>
              <p class="p-hm-p"><?php the_field('black_text3'); ?></p>
            </div>
            <div class="p-flex__elem p-flexMargin">
              <div class="c-media">
    <?php 
    $image = get_field('image2_pc');
    if($image):
    ?>
                <img class="c-media__image pc" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image2_pc, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>

    <?php 
    $image = get_field('image2_sp');
    if($image):
    ?>
                <img class="c-media__image sp" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image2_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
              <div class="figue__caption"><?php the_field('image2_description'); ?></div>
            </div>
          </div>
<?php endif; ?>


<?php if(get_field('black_text4')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <p class="p-hm-p"><?php the_field('black_text4'); ?></p>
          </div>
<?php endif; ?>


<?php if(get_field('headline4') || get_field('black_text5')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <h3 class="p-hm-h3"><?php the_field('headline4'); ?></h3>
            <p class="p-hm-p"><?php the_field('black_text5'); ?></p>
          </div>
<?php endif; ?>


<?php if(get_field('headline5') || get_field('black_text6') || get_field('image3_pc') || get_field('image3_sp') || get_field('image3_description')): ?>
          <div class="p-flex p-flex--reverse wow fadeIn" data-wow-delay="0.5s">
            <div class="p-flex__elem p-flexMargin p-flex__elem p-flexMargin--top0">
              <h3 class="p-hm-h3"><?php the_field('headline5'); ?></h3>
              <p class="p-hm-p"><?php the_field('black_text6'); ?></p>
            </div>
            <div class="p-flex__elem p-flexMargin">
              <div class="c-media">
    <?php 
    $image = get_field('image3_pc');
    if($image):
    ?>
                <img class="c-media__image pc" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image3_pc, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>

    <?php 
    $image = get_field('image3_sp');
    if($image):
    ?>
                <img class="c-media__image sp" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image3_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
              <div class="figue__caption"><?php the_field('image3_description'); ?></div>
            </div>
          </div>
<?php endif; ?>


<?php if(get_field('black_text7')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <p class="p-hm-p"><?php the_field('black_text7'); ?></p>
          </div>
<?php endif; ?>


<?php if(get_field('headline6') || get_field('black_text8')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <h3 class="p-hm-h3"><?php the_field('headline6'); ?></h3>
            <p class="p-hm-p"><?php the_field('black_text8'); ?></p>
          </div>
<?php endif; ?>


<?php if(get_field('headline7') || get_field('black_text9') || get_field('image4_pc') || get_field('image4_sp') || get_field('image4_description')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <div class="p-flex__elem p-flexMargin">
              <h3 class="p-hm-h3"><?php the_field('headline7'); ?></h3>
              <p class="p-hm-p"><?php the_field('black_text9'); ?></p>
            </div>
            <div class="p-flex__elem p-flexMargin">
              <div class="c-media">
    <?php 
    $image = get_field('image4_pc');
    if($image):
    ?>
                <img class="c-media__image pc" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image4_pc, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>

    <?php 
    $image = get_field('image4_sp');
    if($image):
    ?>
                <img class="c-media__image sp" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image4_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
              <div class="figue__caption"><?php the_field('image4_description'); ?></div>
            </div>
          </div>
<?php endif; ?>


<?php if(get_field('black_text10')): ?>
          <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <p class="p-hm-p"><?php the_field('black_text10'); ?></p>
          </div>
<?php endif; ?>


<?php if(get_field('black_text11')): ?>
           <div class="p-flex wow fadeIn" data-wow-delay="0.5s">
            <p class="p-hm-p font-ica"><?php the_field('black_text11'); ?></p>
          </div>
<?php endif; ?>


        </section>


        <?php get_template_part('profile'); ?>

        <section class="contents-section">

          <?php include locate_template('pager.php'); ?>

　        <?php get_template_part('nav-sns'); ?>
        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->
        </section>
      </div>


    </main>
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  

</body>
<?php
//article area end
endwhile;
?>
</html>