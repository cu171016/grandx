<!--
/*
Template Name: あの人 50音順
Template Post Type: page
*/
-->
<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX,グランエックス,タイガー魔法瓶,土鍋圧力炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー,人,あの人">
<meta name="description" content="タイガーGRAND X(グランエックス)シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[あの人]">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="https://www.grandx.jp/human/">
<meta property="og:type" content="website">
<meta property="og:title" content="あの人｜GX GRANDX クラブ｜タイガー魔法瓶">
<meta property="og:description" content="タイガーGRAND X(グランエックス)シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[あの人]">
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/common/ogp.png">
<meta property="og:site_name" content="あの人｜GX GRANDX クラブ｜タイガー魔法瓶">

<title>あの人｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5b8a7e780eec4098b0320d8bac1e02ec" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li><a href="/human/">あの人</a></li>
        <li class="active">あの人（50音順）</li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <main role="main" class="main-content">
      <div class="content-inner detail category-index child human-50on">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->

        <h1 class="h1-contents-title">あの人</h1>

        <div class="selecter">
          <a class="btn-category" href="/human/">テーマ別</a>
          <a class="btn-category current" href="/human/50on/">50音順</a>
        </div>

        <section class="contents-section">
          <p id="page-navi"><a href="#a">あ</a>　<a href="#ka">か</a>　<a href="#sa">さ</a>　<a href="#">た</a>　<a href="#na">な</a>　<a href="#ha">は</a>　<a href="#">ま</a>　<a href="#ya">や</a>　<a href="#">ら</a>　<a href="#wa">わ</a></p>
          <div id="a">
            <h3>あ</h3>

            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => '石村由起子'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_ishimura.jpg" alt="石村由起子（カフェギャラリー・ホテルレストランオーナー）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_ishimura-sp.jpg" alt="石村由起子（カフェギャラリー・ホテルレストランオーナー）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>石村由起子<small>（カフェギャラリー・ホテルレストランオーナー）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>

            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => 'ウー・ウェン'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_wu.jpg" alt="ウー・ウェン（料理研究家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_wu-sp.jpg" alt="ウー・ウェン（料理研究家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>ウー・ウェン<small>（料理研究家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => 'おさだゆかり'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_osada.jpg" alt="おさだゆかり（ショップオーナー）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_osada-sp.jpg" alt="おさだゆかり（ショップオーナー）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>おさだゆかり<small>（ショップオーナー）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
          </div>
          <div id="ka">
            <h3>か</h3>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => '小堀紀代美'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol29/thumb_pc.jpg" alt="小堀紀代美（料理家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol29/thumb_sp.jpg" alt="小堀紀代美（料理家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>小堀紀代美<small>（料理家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
          </div>
          <div id="sa">
            <h3>さ</h3>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => 'スズキエミ'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_suzuki.jpg" alt="スズキエミ（料理家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_suzuki-sp.jpg" alt="スズキエミ（料理家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>スズキエミ<small>（料理家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
          </div>
          <div id="na">
            <h3>な</h3>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => '中川たま'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_nakagawa.jpg" alt="中川たま（料理家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_nakagawa-sp.jpg" alt="中川たま（料理家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>中川たま<small>（料理家）</small></h4>
                <h5>＜全10回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>><?php the_field('publish'); ?><a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
          </div>
          <div id="ha">
            <h3>は</h3>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => 'パン・ウェイ'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol41/thumb_pc.jpg" alt="パン・ウェイ（料理研究家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol41/thumb_sp.jpg" alt="パン・ウェイ（料理研究家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>パン・ウェイ<small>（料理研究家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => '飛田和緒'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_hida.jpg" alt="飛田 和緒（料理家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_hida-sp.jpg" alt="飛田 和緒（料理家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>飛田 和緒<small>（料理家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => '平井かずみ'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_hirai.jpg" alt="平井かずみ（フラワースタイリスト）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_hirai-sp.jpg" alt="平井かずみ（フラワースタイリスト）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>平井かずみ<small>（フラワースタイリスト）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
            <div>
            <?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => 'ホルトハウス房子'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_hoult.jpg" alt="ホルトハウス房子（料理家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_hoult-sp.jpg" alt="ホルトハウス房子（料理家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>ホルトハウス房子<small>（料理家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
          </div>
          <div id="ya">
            <h3>や</h3>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => '山戸ユカ'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_yamato.jpg" alt="山戸ユカ（料理研究家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_yamato-sp.jpg" alt="山戸ユカ（料理研究家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>山戸ユカ<small>（料理研究家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
          </div>

          <div id="wa">
            <h3>わ</h3>
            <div>
<?php
$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => 'ワタナベマキ'
        );
$the_query = new WP_Query($args);
$imglink = '';
$imglinkend = '';
if($the_query->have_posts()):
    // ここで一つ目の記事のURL取得
    $posts = $the_query->get_posts();
    $imglink = '<a href="' .get_permalink($posts[0]->ID).'">';
    $imglinkend = '</a>';
endif;
?>
              <div class="left-box">
                <p>
                  <?php echo $imglink; ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_watanabe.jpg" alt="ワタナベマキ（料理家）" class="pc">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/human/thumb_watanabe-sp.jpg" alt="ワタナベマキ（料理家）" class="sp">
                  <?php echo $imglinkend; ?>
                </p>
              </div>
              <div class="right-box">
                <h4>ワタナベマキ<small>（料理家）</small></h4>
                <h5>＜全3回＞</h5>
                <ul>
<?php
if($the_query->have_posts()):
    $cnt = 1;
    while($the_query->have_posts()): $the_query->the_post();
        $posted = get_the_date('Y-m-d');
        $today = date("Y-m-d");
        $new = '';
        if (day_diff($today, $posted) <= 5) {
            $new = ' class="new"';
        }
?>
                  <li<?php echo $new; ?>>第<?php echo $cnt; ?>話<a href="<?php echo get_permalink(); ?>"><?php the_field('category_title'); ?></a></li>
<?php
      $cnt++;
    endwhile;
endif;
wp_reset_postdata();
?>
                </ul>
              </div>
            </div>
          </div>
          
        </section>

        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->
      </div>
    </main>
    <?php get_template_part('nav-sns'); ?>
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>