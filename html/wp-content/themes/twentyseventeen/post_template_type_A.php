<!--
/*
Template Name: あの人 A あの人が「うん」と言う
Template Post Type: post, page
*/
-->
<!DOCTYPE HTML>
<html>
<?php
// article area start
while ( have_posts() ) : the_post();
    $currentId = get_the_ID();
    $who = get_field('who');
    $category = get_the_category();
    $cat_name = $category[0]->cat_name;
    $cat_slug = $category[0]->category_nicename;
?>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="<?php the_field('meta_keywords'); ?>">
<meta name="description" content="<?php the_field('meta_description'); ?>">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="<?php echo get_permalink(get_the_ID()); ?>">
<meta property="og:type" content="website">
<meta property="og:title" content="<?php the_field('ogp_title'); ?>">
<meta property="og:description" content="<?php the_field('ogp_description'); ?>">
<meta property="og:image" content="<?php the_field('ogp_image'); ?>">
<meta property="og:site_name" content="<?php the_field('ogp_site_name'); ?>">

<title><?php the_field('who'); ?>さん｜あの人｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5b8a7e780eec4098b0320d8bac1e02ec" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li><a href="/<?php echo $cat_slug; ?>/"><?php echo $cat_name; ?></a></li>
        <li class="active">#<?php the_field('number'); ?> <?php the_title(); ?></li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <style>
<?php if( $post->main_image_pc ) : ?>
      .cover {
        background: url("<?php the_field('main_image_pc'); ?>") center center no-repeat;
        background-size: cover; }
<?php endif; ?>
<?php if( $post->main_image_sp ) : ?>
      @media only screen and (max-width: 768px) {
        .cover {
          background: url("<?php the_field('main_image_sp'); ?>") center center no-repeat;
          background-size: cover; }
      }
<?php endif; ?>
    </style>

    <div class="cover"></div>

    <main role="main" class="main-content">
      <div class="main-content__inner">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->

        <section class="contents-section contents-section--plarge">

          <h1 class="p-h2cache p-h2cache--hmv1"><?php the_title(); ?><br><span class="p-h2cache--hmv8__name"><?php the_field('who'); ?><span>さん</span></span></h1>
          <p class="p-hmVol-num"><?php the_field('number'); ?></p>




<?php if(get_field('gray_text1') || get_field('black_text1') || get_field('image1_pc') || get_field('image1_sp') || get_field('image1_description')): ?>
          <div class="p-flex">
            <div class="p-flex__elem p-flexMargin">
    <?php if(get_field('gray_text1')): ?>
              <h3 class="cache-g cache-g--large p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('gray_text1'); ?></h3>
    <?php endif; ?>

    <?php if( get_field('black_text1') ): ?>
              <p class="p-hm-p wow fadeIn font-ica" data-wow-delay="0.5s"><?php the_field('black_text1'); ?></p>
    <?php endif; ?>
            </div>

            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media p-hm-thumb--posDown">
    <?php 
    $image = get_field('image1_sp');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image1_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if(get_field('image1_description')): ?>
              <div class="figue__caption"><?php the_field('image1_description'); ?></div>
    <?php endif; ?>
            </div>
          </div>
<?php endif; ?>



<?php if(get_field('gray_text2') || get_field('black_text2') || get_field('gray_text3') || get_field('black_text3') || get_field('image2_pc') || get_field('image2_sp') || get_field('image2_description')): ?>
          <div class="p-flex p-flex--reverse">
            <div class="p-flex__elem p-flexMargin">
    <?php if(get_field('gray_text2')): ?>
              <p class="cache-g p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('gray_text2'); ?></p>
    <?php endif; ?>

    <?php if( get_field('black_text2') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text2'); ?></p>
    <?php endif; ?>

    <?php if(get_field('gray_text3')): ?>
              <p class="cache-g p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('gray_text3'); ?></p>
    <?php endif; ?>

    <?php if( get_field('black_text3') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text3'); ?></p>
    <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin wow fadeIn" data-wow-delay="0.5s">
              <div class="c-media p-hm-thumb--large p-hm-thumb--large-left">
    <?php 
    $image = get_field('image2_pc');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image2_pc, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if(get_field('image2_description')): ?>
              <div class="figue__caption"><?php the_field('image2_description'); ?></div>
    <?php endif; ?>
            </div>
          </div>
<?php endif; ?>



<?php if(get_field('image3_pc') || get_field('image3_sp') || get_field('image3_description') || get_field('question1') || get_field('answer1') || get_field('question2') || get_field('answer2')): ?>
          <div class="p-flex">
            <div class="p-flex__elem p-flexMargin">

              <div class="c-media p-hm-thumb--posUp wow fadeIn" data-wow-delay="0.5s">
    <?php 
    $image = get_field('image3_sp');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image3_sp, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if(get_field('image3_description')): ?>
              <div class="figue__caption"><?php the_field('image3_description'); ?></div>
    <?php endif; ?>
    
    <?php if( get_field('question1') ): ?>
              <p class="p-hm-comment wow fadeIn" data-wow-delay="0.5s"><span><?php the_field('question1'); ?></span></p>
    <?php endif; ?>
    
    <?php if( get_field('answer1') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('answer1'); ?></p>
    <?php endif; ?>
    
    <?php if( get_field('question2') ): ?>
              <p class="p-hm-comment wow fadeIn" data-wow-delay="0.5s"><span><?php the_field('question2'); ?></span></p>
    <?php endif; ?>
    
    <?php if( get_field('answer2') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('answer2'); ?></p>
    <?php endif; ?>
            </div>
<?php endif; ?>
    
    
    
    
<?php if(get_field('image4_pc') || get_field('image4_sp') || get_field('image4_description') || get_field('question3') || get_field('answer3') || get_field('gray_text4') || get_field('black_text4')): ?>
            <div class="p-flex__elem p-flexMargin">

              <div class="c-media p-hm-thumb--large p-hm-thumb--large-right wow fadeIn" data-wow-delay="0.5s">
    <?php 
    $image = get_field('image4_pc');
    if($image):
    ?>
                <img class="c-media__image" src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->image4_pc, '_wp_attachment_image_alt', true ); ?>">
    <?php endif; ?>
              </div>
    <?php if(get_field('image4_description')): ?>
              <div class="figue__caption"><?php the_field('image4_description'); ?></div>
    <?php endif; ?>

    <?php if( get_field('question3') ): ?>
              <p class="p-hm-comment wow fadeIn" data-wow-delay="0.5s"><span><?php the_field('question3'); ?></span></p>
    <?php endif; ?>

    <?php if( get_field('answer3') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('answer3'); ?></p>
    <?php endif; ?>

    <?php if(get_field('gray_text4')): ?>
              <p class="cache-g p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('gray_text4'); ?></p>
    <?php endif; ?>

    <?php if( get_field('black_text4') ): ?>
              <p class="p-hm-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text4'); ?></p>
    <?php endif; ?>
            </div>
          </div>
<?php endif; ?>

<?php get_template_part( 'template-parts/post/column' ); ?>

<?php if( get_field('credit') ): ?>
          <p class="p-credit wow fadeIn" data-wow-delay="0.5s"><?php the_field('credit'); ?></p>
<?php endif; ?>

        </section>

        <?php get_template_part('profile'); ?>

        <section class="contents-section section3">

          <?php include locate_template('pager.php'); ?>

　        <?php get_template_part('nav-sns'); ?>

        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->
        </section>
      </div>


    </main>
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
<?php
//article area end
endwhile;
?>
</html>