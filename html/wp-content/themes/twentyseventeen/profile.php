        <section class="p-profile">
          <div class="p-profile__elem">
            <p class="p-profile__head center">＜プロフィール＞</p>
<?php if(get_field('who') == 'ホルトハウス房子'): ?>
            <p class="p-profile__name center"><span>ホルトハウス房子</span><br>（ほるとはうすふさこ）</p>
            <p class="p-profile__txt">料理研究家。1933年、東京都生まれ。アメリカ人の夫とともに、海外で生活するなかで世界中のさまざまな味に親しむ。旬の素材を使った西洋料理、洋菓子を教える料理教室を主宰。自宅の一角で洋菓子店「ハウス オブ フレーバーズ」も営む。著書多数。</p>
<?php elseif(get_field('who') == '中川たま'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol2/prof_img.png" alt="中川たま"></div>
            <p class="p-profile__name center"><span>中川たま</span><br>（なかがわたま）</p>
            <p class="p-profile__txt">料理家。神奈川県・逗子で夫と高校生の娘と暮らす。自然食品店勤務後、ケータリングユニット「にぎにぎ」を経て独立。伝統を受け継ぎながら今の暮らしに寄り添い、季節のエッセンスを加えた手仕事の提案を行う。最新刊は2018年2月刊行の『季節の果実をめぐる114の愛で方、食べ方』（日本文化社）。ほか著書多数。</p>
<?php elseif(get_field('who') == 'ワタナベマキ'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol3/prof_img.png" alt="ワタナベマキ"></div>
            <p class="p-profile__name center"><span>ワタナベマキ</span></p>
            <p class="p-profile__txt">料理家。季節感を大切にした野菜たっぷりの体に優しいごはんや、手をかけながらシンプルに楽しむ保存食など、今の暮らしに合ったセンスの光るレシピが人気。旦那さんと小学5年生の息子さんの３人暮らし。著書に『ワタナベマキのおいしい仕組み：少ない材料・シンプル調理でも絶品になる！味つけのルール 』（日本文芸社）、「アジアのサラダ」（主婦と生活社）など多数。</p>
<?php elseif(get_field('who') == '飛田和緒'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol4/prof_img.png" alt="飛田和緒"></div>
            <p class="p-profile__name center"><span>飛田和緒</span><br> （ひだかずを）</p>
            <p class="p-profile__txt">料理家。高校3年間を長野で過ごし、現在は海辺の町に夫と娘とともに暮らす。大人も子どもも喜ぶ毎日のおかず、季節の素材をいかした常備菜や保存食など、シンプルな味付けの作りやすいレシピが人気。『くりかえし料理』（地球丸）、『海辺暮らし 季節のごはんとおかず』（女子栄養大学出版部）など著書多数</p>
<?php elseif(get_field('who') == '料理家 近藤幸子'): ?>
            <p class="p-profile__name center"><span>近藤幸子</span><br>（こんどうさちこ）</p>
            <p class="p-profile__txt">料理研究家、管理栄養士。料理教室「おいしい週末」主宰。1976年、宮城県生まれ。雑誌や書籍などで活躍中。家族のために作る料理や等身大のライフスタイルが人気。近著に『がんばりすぎないごはん』（主婦と生活社）がある。</p>
            <a href="http://oishisyumatsu.com/" target="_blank">「おいしい週末」</a>
<?php elseif(get_field('who') == 'ウー・ウェン'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol8/prof_img.png" alt="ウー・ウェン"></div>
            <p class="p-profile__name center"><span>ウー・ウェン</span></p>
            <p class="p-profile__txt">料理研究家。中国・北京出身。１９９０年来日。母から受け継いだ家庭料理が評判となり料理研究家の道へ。「料理を通じて日本と中国との架け橋になりたい」という思いから東京と北京でクッキングサロンを主宰する。</p>
<?php elseif(get_field('who') == '平井かずみ'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol11/prof_img.png" alt="平井かずみ"><img class="c-media__image sp" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol11/prof_img-sp.png" alt="平井かずみ"></div>
            <p class="p-profile__name center"><span>平井かずみ</span><br>（ひらいかずみ）</p>
            <p class="p-profile__txt">フラワースタイリスト。「café イカニカ」（東京・自由が丘）を拠点に、花の会やリース教室を全国各地で開催。草花をもっと身近に感じられるような「日常花」の提案をしている。著書に「あなたの暮らしに似合う花」（地球丸）、『ブーケとリース』（主婦の友社）など。<a href="http://ikanika.com/" target="_blank">http://ikanika.com/</a></p>
<?php elseif(get_field('who') == 'スズキエミ'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol9/prof_img.png" alt="スズキエミ"></div>
            <p class="p-profile__name center"><span>スズキエミ</span></p>
            <p class="p-profile__txt">料理家。宮城県石巻生まれ。料理教室「暦ごはんの会」「暦の手しごとの会」主宰。素材の持ち味を生かし、日本の季節を身近に感じられるようなごはん作りを提案している。著書に『四季を味わう にっぽんのパスタ』『野菜の保存食で毎日のごはんがすごく楽になる』(立東舎)など。<br class="sp"><a href="https://www.instagram.com/suzukiemi.gohan/" target="_blank">https://www.instagram.com/suzukiemi.gohan/</a></p>
<?php elseif(get_field('who') == '松浦弥太郎'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/message/prof-img.png" alt="松浦弥太郎"></div>
            <p class="p-profile__name center"><span>松浦弥太郎</span></p>
            <p class="p-profile__txt">元「暮しの手帖」編集長、エッセイスト。1965年東京生まれ。多方面のメディアにてエッセイスト、編集者として活躍。2006年から15年3月まで、約9年間『暮しの手帖』の編集長を務める。その後、「くらしのきほん」編集長。新聞、雑誌の連載の他、ベストセラーに「今日もていねいに」「しごとのきほん　くらしのきほん１００」他多数。NHKラジオ第一「かれんスタイル」のパーソナリティとしても活躍。</p>
<?php elseif(get_field('who') == '相場正一郎'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/recipe_vol3/prof_img.png" alt="相場正一郎"></div>
            <p class="p-profile__name center"><span>相場正一郎</span><br> （あいばしょういちろう）</p>
            <p class="p-profile__txt">東京・渋谷区にあるイタリアンレストラン〈LIFE〉オーナーシェフ。サーフィンや釣り、トレッキングなど自然の中で過ごす時間が好きで、週末は故郷の那須に作った山小屋へ。都市と自然を行き来するライフスタイルも注目を集める。</p>
            <a href="http://www.s-life.jp/" target="_blank">www.s-life.jp</a>
<?php elseif(get_field('who') == 'あゆみ食堂'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/recipe_vol4/prof_img.png" alt="あゆみ食堂"></div>
            <p class="p-profile__name center"><span>あゆみ食堂</span><br> （あゆみしょくどう）</p>
            <p class="p-profile__txt">出張料理 “あゆみ食堂” として、様々なイベント、展示会などでケータリングを行う。料理を食べる人、食べる環境、コンセプトに合わせて作る“オーダーメイドレシピ”も多く手がける。著書に『あゆみ食堂のお弁当 23人の手紙からうまれたレシピ』（文化出版局）。</p>
<?php elseif(get_field('who') == 'おさだゆかり'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol12/prof_img.png" alt="おさだゆかり"></div>
            <p class="p-profile__name center"><span>おさだゆかり</span></p>
            <p class="p-profile__txt">自身で買い付けた北欧雑貨を扱うショップ『SPOONFUL』オーナー。現在はオンラインショップと予約制の実店舗を運営しつつ、全国各地でイベントなども行っている。著書に『北欧雑貨手帖』（アノニマ・スタジオ）など。2018年春にスウェーデンのガイドブックを出版予定。<br class="sp"><a href="http://www.spoon-ful.jp/" target="_blank">http://www.spoon-ful.jp/</a></p>
<?php elseif(get_field('who') == '小堀紀代美'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol29/prof_img.png" alt="小堀紀代美"></div>
            <p class="p-profile__name center"><span>小堀紀代美</span>（こぼりきよみ）</p>
            <p class="p-profile__txt">カフェのような食堂『LIKE LIKE KITCHEN』のオーナーシェフを経て、料理家に。同名の、料理とお菓子の教室を主宰する。著書に『フルーツのサラダ&スイーツ 』（NHK出版）、『スプーンで作るおやつ』（主婦の友社）など。</p>
<?php elseif(get_field('who') == '山戸ユカ'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol30/prof_img.png" alt="山戸ユカ"></div>
            <p class="p-profile__name center"><span>山戸ユカ</span>（やまとゆか）</p>
            <p class="p-profile__txt">料理研究家。東京都出身。2013年に山梨県北杜市に夫ともに移住。レストランDILL eat,lifeを営む。地元産の有機野菜を使い、玄米菜食を中心とした料理を提供する。近著に『DILL EAT,LIFE. COOKING CLASS』（グラフィック社）。</p>
<?php elseif(get_field('who') == '石村由起子'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol34/prof_img.png" alt="石村由起子"></div>
            <p class="p-profile__name center"><span>石村由起子</span>（いしむらゆきこ）</p>
            <p class="p-profile__txt">奈良在住。カフェギャラリー『くるみの木』、ホテルレストラン『秋篠の森「なず菜」』のオーナー。企業の商品開発や町おこしプロジェクトなど幅広く活躍している。著書に「私は夢中で夢を見た」（文藝春秋）などがある。</p>
            <a href="http://www.kuruminoki.co.jp" target="_blank">「くるみの木」</a>
<?php elseif(get_field('who') == 'パン・ウェイ'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol41/prof_img.png" alt="パン・ウェイ"></div>
            <p class="p-profile__name center"><span>パン・ウェイ</span></p>
            <p class="p-profile__txt">中国・北京生まれ。「季節と身体」をテーマに四季に沿った食生活を提唱し、東京・代々木公園にて薬膳料理や中国家庭料理の教室を主宰。 
テレビ出演や講演会の他、企業向けのレシピ開発等でも活躍中。「毎日からだを調える中華スープ」、「中華小菓子」（誠文堂新光社）など著書多数。</p>
            <a href="http://www.pan-chan.com/" target="_blank">「大陸情韻」</a>
<?php elseif(get_field('who') == 'ツレヅレハナコ'): ?>
            <div class="c-media p-profile__img"><img class="c-media__image pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/human/vol19/prof_img.png" alt="ツレヅレハナコ"></div>
            <p class="p-profile__name center"><span>ツレヅレハナコ</span></p>
            <p class="p-profile__txt">1976年生まれ。寝ても覚めても、おいしい料理とお酒のことをばかり考えている編集者。著書に『女ひとりの夜つまみ』（幻冬舎）、『ツレヅレハナコのじぶん弁当』（小学館）など。最新レシピ本は『ツレヅレハナコの揚げ物天国』（PHP研究所）。</p>
<?php endif; ?>
          </div>
        </section>