<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX ,グランエックス,タイガー魔法瓶,土鍋圧力IH炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー,今日のグランエックス">
<meta name="description" content="GRAND X（グランエックス）シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[今日のグランエックス]">
<link rel="shortcut icon" type="image/x-icon" href="/assets/images/common/favicon.ico">

<?php
  //ターム情報取得
  $terms  = wp_get_object_terms( $post->ID, 'use_person' );
  $term_id = $terms[0]->term_id;
  //OG画像取得
  $ogImage = get_field('ogimage','use_person_'.$term_id);
  if( !$ogImage ) {
    $ogImage = get_template_directory_uri() . '/assets/images/common/ogp.png';
  }
?>
<meta property="og:url" content="https://<?php echo $_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI']; ?>">
<meta property="og:type" content="website">
<meta property="og:title" content="<?php echo get_field('ogtitle','use_person_'.$term_id) ?>">
<meta property="og:description" content="<?php echo rtrim(strip_tags(term_description( $term_id, 'use_person' ))) ?>">
<meta property="og:image" content="<?php echo $ogImage; ?>">
<meta property="og:site_name" content="今日のグランエックス｜GX GRANDX クラブ｜タイガー魔法瓶">

<title>今日のグランエックス｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/accessor/script/typesquare.js?fGhM~60XsvE%3D" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->
<style>
.use .p-use-list section {
  margin-top: -110px;
  padding-top: 170px;
}
@media only screen and (max-width: 768px) {
  .use .p-use-list section {
    margin-top: -60px;
    padding-top: 85px;
  }
}
</style>
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li><a href="/product/">商品アイテム</a></li>
        <li><a href="/product/use/">今日のグランエックス</a></li>
        <li class="active"><?php single_term_title(); ?></li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <main role="main" class="main-content use use-hito-index">
      <div class="content-inner detail">

        <h1 class="h1-contents-title">今日のグランエックス</h1>
        <p class="p-link-top"><a href="/product/use/">TOPへ戻る</a></p>
        <section class="p-hito-profile">
          <div class="p-profile-inner">
            <div class="c-media">
            <?php
              $imgid = get_field('faceimg','use_person_'.$term_id);
              $image = wp_get_attachment_image_src($imgid, 'full');
              $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
            ?>
            <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>">
            <!-- <img src="/assets/images/product/use/hito01.jpg" alt=""></div> -->
            <p class="p-profile-name"><?php echo $terms[0]->name; ?><br>
            <span>（<?php the_field('kana','use_person_'.$term_id) ?>）</span></p>
            <p class="p-profile-text"><?php the_field('profile','use_person_'.$term_id) ?></p>
          </div>
        </section>

        <article class="p-use-list">
<?php
if ( have_posts() ) : 
  /** 新着3件アーカイブページ */
  if (strstr($_SERVER['REQUEST_URI'], 'product/use/hito/archive') === FALSE) {
    global $wp_query;
    while ( have_posts() ) : the_post();
      if ($wp_query->current_post === 10 ) {
?>
        </article>
        <section class="p-thumb-list">
          <ul>
<?php 
      }
      if ( $wp_query->current_post <= 9 ) {
        // 10件目までは全コンテンツ表示
        get_template_part( 'template-parts/use/content' );
      } else {
        // 以降はサムネイルのみ
        $postid = get_the_ID();
?>
            <li>
              <a href="/product/use/hito/archive/<?php echo $terms[0]->slug; ?>/#post<?php echo $postid; ?>">
<?php 
        if ( '' !== get_the_post_thumbnail() ) { 
          echo '<img src="'.get_the_post_thumbnail_url( $postid , 'use_thumbnail_pc' ).'" alt="'.get_post_meta( get_post_thumbnail_id( $postid ), '_wp_attachment_image_alt', true ).'" class="pc">';
          echo '<img src="'.get_the_post_thumbnail_url( $postid , 'use_thumbnail_sp' ).'" alt="'.get_post_meta( get_post_thumbnail_id( $postid ), '_wp_attachment_image_alt', true ).'" class="sp">';
        }
?>
              </a>
            </li>
<?php
      }
    endwhile;

  /** 全一覧ページ */
  } else {
    while ( have_posts() ) : the_post();
      get_template_part( 'template-parts/use/content' );
    endwhile;
  }

endif; 
?>
          </ul>
        </section>

<?php get_template_part( 'template-parts/use/contentfooter' ); ?>
        
      </div>
    </main>
    <?php get_template_part('nav-sns'); ?>
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>