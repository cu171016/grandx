<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>

<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->


<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX ,グランエックス,タイガー魔法瓶,土鍋圧力IH炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー">
<meta name="description" content="GRAND X（グランエックス）シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[TOP]">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="https://www.grandx.jp/index.html">
<meta property="og:type" content="website">
<meta property="og:title" content="GX GRANDX クラブ｜タイガー魔法瓶">
<meta property="og:description" content="GRAND X（グランエックス）シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜">
<meta property="og:image" content="https://www.grandx.jp/assets/images/common/ogp.png">
<meta property="og:site_name" content="GX GRANDX クラブ｜タイガー魔法瓶">

<title>GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/slick.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/slick-theme.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/accessor/script/typesquare.js?fGhM~60XsvE%3D" charset="utf-8"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/createjs-2015.11.26.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index03.js"></script>


<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


  <div class="wrap">
    
    <header role="banner" class="header animated">
      <?php get_header(); ?>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->
    
    <div class="main-visual-container">
      <div class="main-visual">
        <canvas class="pc" id="main-carousel" width="1400" height="500"></canvas>
        
        <div class="slide-list">
<?php
$items = wp_get_nav_menu_items('スライドバナー');
foreach ($items as $item) {
    $imageSrc = wp_get_attachment_image_src($item->thumbnail_id, 'full');
    printf("<div><img src=\"%s\" alt=\"%s\" data-href=\"%s\" data-tagmanager=\"%s\"></div>\n", $imageSrc[0], $item->title, $item->url, $item->title);
}
?>
        </div>

        <div class="slide-list-sp sp">
<?php
$items = wp_get_nav_menu_items('スライドバナー');
foreach ($items as $item) {
    $imageSrc = wp_get_attachment_image_src($item->thumbnail_hover_id, 'full');
    printf("<div><a href=\"%s\" onclick=\"ga('send','event','top','click','メインバナー＞%s');\"><img src=\"%s\" alt=\"%s\"></a></div>\n", $item->url, $item->title, $imageSrc[0], $item->title);
}
?>
        </div>
      

        <div id="news">
          <div class="news_inner">
            <h2 id="grandx_news"><img class="pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/top/news_title.png" alt="NEWS" /><img class="sp" src="<?php echo get_template_directory_uri(); ?>/assets/images/top/news_title-sp.png" alt="NEWS" /></h2>
            <div id="newsContainer">
              <ul>
<?php
$items = wp_get_nav_menu_items('お知らせ情報');
foreach ($items as $item) {
    if ($item->url == '/') {
        printf('<li>%s</li>', $item->title);
    } else {
        printf('<li><a href="%s">%s</a></li>', $item->url, $item->title);
    }
}
?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <main role="main" class="main-content top">
      <div class="content-inner">
       
        <div class="message-container">
          <div class="h2-title">
            <h2>メッセージ</h2>
            <p>グランエックスからみなさまへのご挨拶 </p>
          </div>
          <div class="message-accordion">
            <div class="accordion-inner">
              <p class="message-text center">どうしたら料理を、<br>
            　もっとおいしく作れるのでしょうか。<br><br>

            　どうしたら料理を、<br>
            　もっと楽しく作れるのでしょうか。<br><br>

            　からだに良い、安全で安心な料理は、<br>
            　どうしたら作れるのでしょうか。</p>
              <a class="btn-read-more" href="./message/" onclick="ga('send','event','top','click','メッセージ＞続きを読む');">続きを読む</a>
            </div>
            <p class="btn-message-toggl"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/top/btn-message-toggle.png" alt=""></p>
          </div>
        </div>






        <ul class="thumb-container section1">
<?php
$items = wp_get_nav_menu_items('新着情報');
foreach ($items as $key => $item) {
    $imagePc = wp_get_attachment_image_src($item->thumbnail_id, 'full');
    $imageSp = wp_get_attachment_image_src($item->thumbnail_hover_id, 'full');

    $date = new DateTime($item->post_date);
    $today = date("Y-m-d");
    $new = '';
    if (day_diff($today, $date->format('Y-m-d')) <= 10) {
        $new = ' new';
    }

    $size   = 'size-large';
    $ga_head = 'NEW_BIG_';
    $ga_key  = $key + 1;
    if ($key >= 2) {
    	$size = 'size-small';
	    $ga_head = 'NEW_SMALL_';
	    $ga_key  = $key - 1;
	}
?>
          <li class="thumb-block <?php echo $size; ?><?php echo $new; ?> hover-animation">
            <a href="<?php echo $item->url; ?>" onclick="ga('send','event','top','click','新規エリア＞<?php echo $ga_head;printf("%02d", $ga_key); ?>');">
              <div class="thumb">
                <img class="pc" src="<?php echo $imagePc[0]; ?>" alt="">
                <img class="sp" src="<?php echo $imageSp[0]; ?>" alt="">
              </div>
              <h3 class="thumb-title"><?php echo $item->title; ?></h3>
              <p class="thumb-text"><?php echo $item->attr_title; ?></p>
            </a>
          </li>
<?php
}
?>
        </ul>


        <ul class="thumb-container section2 flex">
<?php
$items = wp_get_nav_menu_items('トップページバナー');
foreach ($items as $item) {
    $imagePc = wp_get_attachment_image_src($item->thumbnail_id, 'full');
    $imageSp = wp_get_attachment_image_src($item->thumbnail_hover_id, 'full');
?>
          <li class="thumb-block size-medium hover-animation">
            <a href="<?php echo $item->url; ?>" onclick="ga('send','event','top','click','下段導線＞<?php echo $item->title; ?>');">
              <div class="thumb"><img class="pc" src="<?php echo $imagePc[0]; ?>" alt=""><img class="sp" src="<?php echo $imageSp[0]; ?>" alt=""></div>
              <h3 class="thumb-title"><?php echo $item->title; ?></h3>
              <p class="thumb-text"><?php echo $item->attr_title; ?></p>
            </a>
          </li>

<?php
}
?>
        </ul>
      </div>
    </main>

    <div class="category-nav-container">
      <h2 class="h2-title">キーワードで探すキーワードで探す</h2>
      <div class="category-nav">
        <ul class="content-inner">
          <li><a href="./tag_search/cooking" onclick="ga('send','event','top','click','カテゴリー＞料理');">料理</a></li>
          <li><a href="./tag_search/foods" onclick="ga('send','event','top','click','カテゴリー＞食材');">食材</a></li>
          <li><a href="./tag_search/season" onclick="ga('send','event','top','click','カテゴリー＞旬');">旬</a></li>
          <li><a href="./tag_search/item" onclick="ga('send','event','top','click','カテゴリー＞道具');">道具</a></li>
          <li><a href="./tag_search/human" onclick="ga('send','event','top','click','カテゴリー＞あの人');">あの人</a></li>
          <li><a href="./tag_search/movie" onclick="ga('send','event','top','click','カテゴリー＞動画');">動画</a></li>
          <li><a href="./tag_search/knowledge" onclick="ga('send','event','top','click','カテゴリー＞学び');">学び</a></li>
          <li><a href="./tag_search/living" onclick="ga('send','event','top','click','カテゴリー＞くらし');">くらし</a></li>
          <li><a href="./tag_search/healthy" onclick="ga('send','event','top','click','カテゴリー＞健康');">健康</a></li>
          <li><a href="./tag_search/grandx" onclick="ga('send','event','top','click','カテゴリー＞グランエックス');">グランエックス</a></li>
        </ul>
      </div>
    </div>

    <aside class="nav-products">
      <h2 class="product-title"><img class="thumb pc" src="/assets/images/top/lineup-title.png" alt="GRANDX ラインアップ"><img class="thumb sp" src="/assets/images/top/lineup-title-sp.png" alt="GRANDX ラインアップ"></h2>
      <ul class="content-inner flex">
        <li><a href="./product/lineup/jpg/style.html" onclick="ga('send','event','top','click','ラインナップ＞土鍋圧力IH炊飯ジャー');">
          <div class="thumb"><img src="/assets/images/top/product-img-rice-cooker.jpg" alt="タイガー魔法瓶 Grandxシリーズ 土鍋圧力IH炊飯ジャー（型番：JPG-X100）"></div>
          <p class="product-name">土鍋圧力IH炊飯ジャー<br>JPG-X100</p>
        </a></li>
        <li><a href="./product/lineup/kbd/style.html" onclick="ga('send','event','top','click','ラインナップ＞IHホームベーカリー');">
          <div class="thumb"><img src="/assets/images/top/product-img-home-bakery.jpg" alt="タイガー魔法瓶 Grandxシリーズ IHホームベーカリー（型番：KBD-X100）"></div>
          <p class="product-name">IHホームベーカリー<br>KBD-X100</p>
        </a></li>
        <li><a href="./product/lineup/kax/style.html" onclick="ga('send','event','top','click','ラインナップ＞スチームコンべクションオーブン');">
          <div class="thumb"><img src="/assets/images/top/product-img-oven.jpg" alt="タイガー魔法瓶 Grandxシリーズ スチームコンべクションオーブン（型番：KAX-X130）"></div>
          <p class="product-name">スチームコンべクションオーブン<br>KAX-X130</p>
        </a></li>
        <li><a href="./product/lineup/acq/style.html" onclick="ga('send','event','top','click','ラインナップ＞コーヒーメーカー');">
          <div class="thumb"><img src="/assets/images/top/product-img-cofee-maker.jpg" alt="タイガー魔法瓶 Grandxシリーズ コーヒーメーカー（型番：ACQ-X020）"></div>
          <p class="product-name">コーヒーメーカー<br>ACQ-X020</p>
        </a></li>
      </ul>
    </aside>
  
    <?php get_template_part('nav-sns'); ?>

  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>
