<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX ,グランエックス,タイガー魔法瓶,土鍋圧力IH炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー,おいしい食材のこと">
<meta name="description" content="GRAND X（グランエックス）シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[おいしい食材のこと]">
<link rel="shortcut icon" type="image/x-icon" href="/assets/images/common/favicon.ico">

<?php
  $ogTitle = "";
  $ogDescription = "タイガーGRAND X(グランエックス)シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[おいしい食材のこと]";
  $ogImage = "ogp.png";
/*
  if ($_SERVER['REQUEST_URI'] == "/product/use/item/acq/") {
    $ogTitle = "【コーヒーメーカー】";
    $ogDescription = "グランエックスとの付き合い方はどんなふうに？読んでわかり、見て発見。みなさまから届いたコーヒーメーカーの楽しみ方をご覧ください。";
    $ogImage = "4.png";
  } else if ($_SERVER['REQUEST_URI'] == "/product/use/item/kax/") {
    $ogTitle = "【スチームコンべクションオーブン】";
    $ogDescription = "グランエックスとの付き合い方はどんなふうに？読んでわかり、見て発見。みなさまから届いたスチームコンべクションオーブンの楽しみ方をご覧ください。";
    $ogImage = "3.png";
  } else if ($_SERVER['REQUEST_URI'] == "/product/use/item/kbd/") {
    $ogTitle = "【IHホームベーカリー】";
    $ogDescription = "グランエックスとの付き合い方はどんなふうに？読んでわかり、見て発見。みなさまから届いたIHホームベーカリーの楽しみ方をご覧ください。";
    $ogImage = "2.png";
  } else if ($_SERVER['REQUEST_URI'] == "/product/use/item/jpg/") {
    $ogTitle = "【土鍋圧力IH炊飯ジャー】";
    $ogDescription = "グランエックスとの付き合い方はどんなふうに？読んでわかり、見て発見。みなさまから届いた土鍋圧力IH炊飯ジャーの楽しみ方をご覧ください。";
    $ogImage = "1.png";
  }
*/ ?>
<meta property="og:url" content="<?php echo "https://" . $_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI']; ?>">
<meta property="og:type" content="website">
<meta property="og:title" content="おいしい食材のこと<?php echo $ogTitle; ?>｜GX GRANDX クラブ｜タイガー魔法瓶">
<meta property="og:description" content="<?php echo $ogDescription; ?>">
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/common/<?php echo $ogImage; ?>">
<meta property="og:site_name" content="おいしい食材のこと｜GX GRANDX クラブ｜タイガー魔法瓶">

<title>おいしい食材のこと｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/accessor/script/typesquare.js?fGhM~60XsvE%3D" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->
<style>
.foods .p-foods-list section {
  margin-top: -110px;
  padding-top: 170px;
}
@media only screen and (max-width: 768px) {
  .foods .p-foods-list section {
    margin-top: -60px;
    padding-top: 85px;
  }
}
</style>
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li><a href="/product/">商品アイテム</a></li>
        <li><a href="/product/oishii/">おいしい食材のこと</a></li>
        <li class="active"><?php single_term_title(); ?></li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <main role="main" class="main-content foods foods-season-index">
      <div class="content-inner detail">

        <h1 class="h1-contents-title">おいしい食材のこと</h1>
        <p class="p-link-top"><a href="/product/oishii/">TOPへ戻る</a></p>
<?php
  $terms  = wp_get_object_terms( $post->ID, 'oishii_season' );
  $term_id = $terms[0]->term_id;
?>
        <section class="p-season-title">
          <div class="p-sesaon-inner">
            <div class="c-media">
              <?php
                $imgid = get_field('season_img','oishii_season_'.$term_id);
                $image = wp_get_attachment_image_src($imgid, 'full');
                $alt = get_post_meta($imgid, '_wp_attachment_image_alt', true);
              ?>
              <img src="<?php echo $image[0] ?>" alt="<?php echo $alt ?>">
            </div>
          </div>
        </section>
        
<?php
if ( have_posts() ) : 
  global $wp_query;
?>
        <article class="p-foods-list">
<?php
  /** アーカイブページ ・カテゴリTOPページ(サムネイル表示無) */
  if ((strstr($_SERVER['REQUEST_URI'], 'product/oishii/season/archive') !== FALSE) || ($wp_query->found_posts < 10)) {
    while ( have_posts() ) : the_post();
      get_template_part( 'template-parts/oishii/content' );
    endwhile;
?>
        </article>
<?php 
  /** カテゴリTOPページ(サムネイル表示有) */
  } else {
    while ( have_posts() ) : the_post();
      if ($wp_query->current_post === 10 ) {
?>
        </article>
        <section class="p-thumb-list">
          <ul>
<?php 
      }
      if ( $wp_query->current_post < 10 ) {
        // 10件目までは全コンテンツ表示
        get_template_part( 'template-parts/oishii/content' );
      } else {
        // 以降はサムネイルのみ
        $postid = get_the_ID();
?>
            <li>
              <a href="/product/oishii/season/archive/<?php echo $terms[0]->slug; ?>/#post<?php echo $postid; ?>">
<?php 
        if ( '' !== get_the_post_thumbnail() ) { 
          echo '<img src="'.get_the_post_thumbnail_url( $postid , 'use_thumbnail_pc' ).'" alt="'.get_post_meta( get_post_thumbnail_id( $postid ), '_wp_attachment_image_alt', true ).'" class="pc">';
          echo '<img src="'.get_the_post_thumbnail_url( $postid , 'use_thumbnail_sp' ).'" alt="'.get_post_meta( get_post_thumbnail_id( $postid ), '_wp_attachment_image_alt', true ).'" class="sp">';
        }
?>
              </a>
            </li>
<?php
      }
    endwhile;
    if ($wp_query->found_posts > 10 ) {
?>
          </ul>
        </section>
<?php
    } else {
?>
        </article>
<?php
    }
  }
endif;
?>

<?php get_template_part( 'template-parts/oishii/contentfooter' ); ?>
        
      </div>
    </main>
    <?php get_template_part('nav-sns'); ?>
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>