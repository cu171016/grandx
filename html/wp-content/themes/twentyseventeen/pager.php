
          <div class="p-pager p-pager--center">
<?php
wp_reset_postdata();

$args = Array(
            'orderby' => 'post-date',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'meta_key' => 'who',
            'meta_value' => $who
        );
$the_query = new WP_Query($args);
$idList = array();
if($the_query->have_posts()):
    if ($the_query->found_posts == 1) {
?>
            <div class="p-pager__elem is-disable">
              <a class="p-pager__btn p-pager__btn--back" href="#"></a>
              <p class="p-pager__txt">前の記事へ</p>
            </div>
            <div class="p-pager__elem is-disable">
              <a class="p-pager__btn p-pager__btn--next" href="#"></a>
              <p class="p-pager__txt">次の記事へ</p>
            </div>
<?php
    } else {
        while($the_query->have_posts()): $the_query->the_post();
            $idList[] = get_the_ID();
        endwhile;
        $currentKey = array_search($currentId, $idList);
        $preKey = $currentKey - 1;
        if (isset($idList[$preKey])) {
?>
            <div class="p-pager__elem">
              <a class="p-pager__btn p-pager__btn--back" href="<?php echo get_permalink($idList[$preKey]); ?>"></a>
              <p class="p-pager__txt">前の記事へ</p>
            </div>
<?php
        } else {
?>
            <div class="p-pager__elem is-disable">
              <a class="p-pager__btn p-pager__btn--back" href="#"></a>
              <p class="p-pager__txt">前の記事へ</p>
            </div>
<?php
        }

        $nextKey = $currentKey + 1;
        if (isset($idList[$nextKey])) {
?>
            <div class="p-pager__elem">
              <a class="p-pager__btn p-pager__btn--next" href="<?php echo get_permalink($idList[$nextKey]); ?>"></a>
              <p class="p-pager__txt">次の記事へ</p>
            </div>
<?php
        } else {
?>
            <div class="p-pager__elem is-disable">
              <a class="p-pager__btn p-pager__btn--next" href="#"></a>
              <p class="p-pager__txt">次の記事へ</p>
            </div>
<?php
        }


    }
endif;
wp_reset_postdata();
?>
          </div>

