<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX,グランエックス,タイガー魔法瓶,土鍋圧力炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー,今日のグランエックス">
<meta name="description" content="タイガーGRAND X(グランエックス)シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[今日のグランエックス]">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="https://www.grandx.jp/product/use/">
<meta property="og:type" content="website">
<meta property="og:title" content="今日のグランエックス｜GX GRANDX クラブ｜タイガー魔法瓶">
<meta property="og:description" content="グランエックスとの付き合い方はどんなふうに？読んでわかり、見て発見、いろいろな楽しみ方があるんです。みなさまから届いた「今日のグランエックス」をご覧ください。">
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/common/ogp.png">
<meta property="og:site_name" content="今日のグランエックス｜GX GRANDX クラブ｜タイガー魔法瓶">

<title>今日のグランエックス｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5b8a7e780eec4098b0320d8bac1e02ec" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li><a href="/product/">商品アイテム</a></li>
        <li class="active">今日のグランエックス</li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <main role="main" class="main-content use use-index">
      <div class="content-inner detail">
        <h1 class="h1-contents-title">今日のグランエックス</h1>
        <p class="p-top-caption">あの方この方のお宅で愛用されているグランエックス。<br>
        さてさて、グランエックスとの付き合い方はどんなふうに？読んでわかり、見て発見、いろいろな楽しみ方があるんです。<br>
        みなさまから届いた「今日のグランエックス」をご覧ください。（毎週火曜更新） </p>
        <article class="p-use-list">
          <?php
          if ( have_posts() ) : 
            while ( have_posts() ) : the_post();
              get_template_part( 'template-parts/use/content' );
            endwhile;
          endif; 
          ?>
        </article>


<?php get_template_part( 'template-parts/use/contentfooter' ); ?>


      </div>
    </main>
    <?php get_template_part('nav-sns'); ?>
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>