<!--
/*
Template Name: レシピ E 焼売
Template Post Type: post, page
*/
-->
<!DOCTYPE HTML>
<html>
<?php
// article area start
while ( have_posts() ) : the_post();
    $currentId = get_the_ID();
    $who = get_field('who');
    $category = get_the_category();
    $cat_name = $category[0]->cat_name;
    $cat_slug = $category[0]->category_nicename;
?>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="<?php the_field('meta_keywords'); ?>">
<meta name="description" content="<?php the_field('meta_description'); ?>">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="<?php echo get_permalink(get_the_ID()); ?>">
<meta property="og:type" content="website">
<meta property="og:title" content="<?php the_field('ogp_title'); ?>">
<meta property="og:description" content="<?php the_field('ogp_description'); ?>">
<meta property="og:image" content="<?php the_field('ogp_image'); ?>">
<meta property="og:site_name" content="<?php the_field('ogp_site_name'); ?>">

<title><?php the_title(); ?> #<?php the_field('number'); ?> <?php the_field('menu'); ?>｜<?php echo $cat_name; ?>｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/3/tsst/script/ja/typesquare.js?5b8a7e780eec4098b0320d8bac1e02ec" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
      	<li><a href="/">トップページ</a></li>
      	<li><a href="/<?php echo $cat_slug; ?>/"><?php echo $cat_name; ?></a></li>
      	<li class="active">#<?php the_field('number'); ?> <?php the_field('menu'); ?></li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <style>
<?php if( $post->main_image_pc ) : ?>
      .cover {
        background: url("<?php the_field('main_image_pc'); ?>") center center no-repeat;
        background-size: cover; }
<?php endif; ?>
<?php if( $post->main_image_sp ) : ?>
      @media only screen and (max-width: 768px) {
        .cover {
          background: url("<?php the_field('main_image_sp'); ?>") center center no-repeat;
          background-size: cover; }
      }
<?php endif; ?>
    </style>

    <div class="cover"></div>

    <main role="main" class="main-content">
      <div class="main-content__inner">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->
        
        <h1 class="p-rcp-h2-coverTitle">
          <span class="sub-cpoy"><?php the_field('copy'); ?></span>
          <span class="copy-id"><?php the_field('number'); ?></span><span class="main-copy"><?php the_field('menu'); ?></span>
        </h1>
        
        <div class="p-rcp-boxLabel wow fadeIn" data-wow-delay="0.5s">
          <p class="recipe-title"><?php the_title(); ?></p>
          <p class="recipe-copy"><?php the_field('who'); ?><span>さん</span></p>
        </div>

<?php if(get_field('gray_text1') || get_field('black_text1') || get_field('black_text2')): ?>
        <section class="contents-section contents-section--section2">
          <div class="p-flex">
            <div class="p-flex__elem p-flexMargin">
    <?php if(get_field('gray_text1')): ?>
              <p class="cache-g p-rcp-p wow fadeIn" data-wow-delay="0.5s"><?php the_field('gray_text1'); ?></h3>
    <?php endif; ?>

    <?php if(get_field('black_text1')): ?>
              <p class="p-rcp-p wow fadeIn font-ica" data-wow-delay="0.5s"><?php the_field('black_text1'); ?></h3>
    <?php endif; ?>
            </div>
            <div class="p-flex__elem p-flexMargin">
    <?php if(get_field('black_text2')): ?>
              <p class="p-rcp-p p-rcp__firstTxtMatgin wow fadeIn font-ica" data-wow-delay="0.5s"><?php the_field('black_text2'); ?></h3>
    <?php endif; ?>
            </div>
          </div>
        </section>
<?php endif; ?>

        <section class="p-recipe p-recipe--padding">



<?php if(get_field('youtube_url')): ?>
          <div class="p-rcp-recipeMovieWrapper wow fadeIn" data-wow-delay="0.5s">
            <iframe class="recipe-movie" width="800" height="450" src="<?php the_field('youtube_url'); ?>" frameborder="0" allowfullscreen=""></iframe>
          </div>
<?php endif; ?>



          <div class="p-recipe__inner p-rcp__inner border-dot">
            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
                <p class="p-recipe__label p-rcp-zairyo-txt">［<?php the_field('menu'); ?>］</p>
                <dl class="p-rcp-dl-list">
<?php
$material = get_field('material_text');
if($material):
    $list = explode("\n", $material);
    foreach ($list as $value) {
        $parts = explode("：", $value);
        if (count($parts) == 2) {
	        printf("<dt>%s</dt><dd>%s</dd>", $parts[0], $parts[1]);
        } else {
	        printf("<dt class=\"lbl\">%s</dt>", $parts[0]);        	
        }
    }
endif;
?>

                </dl>
              </div>

              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('material_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->material_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
              </figure>
            </div>

            <div class="p-rcp-textThumb p-rcp-border-dot-top wow fadeIn" data-wow-delay="0.5s">
              <p class="p-recipe__label">［作り方］</p>
              <div class="txt">
<?php if(get_field('step1_text')): ?>
                <p class="txt-num1 numbering"><?php the_field('step1_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step1_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step1_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step1_image_description')): ?>
                <figcaption><?php the_field('step1_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>

            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step2_text')): ?>
                <p class="txt-num2 numbering"><?php the_field('step2_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step2_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step2_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step2_image_description')): ?>
                <figcaption><?php the_field('step2_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>

            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step3_text')): ?>
                <p class="txt-num3 numbering"><?php the_field('step3_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step3_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step3_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step3_image_description')): ?>
                <figcaption><?php the_field('step3_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>

            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step4_text')): ?>
                <p class="txt-num4 numbering"><?php the_field('step4_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step4_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step4_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step4_image_description')): ?>
                <figcaption><?php the_field('step4_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>

            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step5_text')): ?>
                <p class="txt-num5 numbering"><?php the_field('step5_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step5_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step5_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step5_image_description')): ?>
                <figcaption><?php the_field('step5_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>

<?php if(get_field('step6_text') || get_field('step6_image_pc') || get_field('step6_image_sp') || get_field('step6_image_description')): ?>
            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step6_text')): ?>
                <p class="txt-num6 numbering"><?php the_field('step6_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step6_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step6_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step6_image_description')): ?>
                <figcaption><?php the_field('step6_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>
<?php endif; ?>

<?php if(get_field('step7_text') || get_field('step7_image_pc') || get_field('step7_image_sp') || get_field('step7_image_description')): ?>
            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step7_text')): ?>
                <p class="txt-num7 numbering"><?php the_field('step7_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step7_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step7_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step7_image_description')): ?>
                <figcaption><?php the_field('step7_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>
<?php endif; ?>

<?php if(get_field('step8_text') || get_field('step8_image_pc') || get_field('step8_image_sp') || get_field('step8_image_description')): ?>
            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step8_text')): ?>
                <p class="txt-num8 numbering"><?php the_field('step8_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step8_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step8_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step8_image_description')): ?>
                <figcaption><?php the_field('step8_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>
<?php endif; ?>

<?php if(get_field('step9_text') || get_field('step9_image_pc') || get_field('step9_image_sp') || get_field('step9_image_description')): ?>
            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step9_text')): ?>
                <p class="txt-num9 numbering"><?php the_field('step9_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step9_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step9_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step9_image_description')): ?>
                <figcaption><?php the_field('step9_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>
<?php endif; ?>

<?php if(get_field('step10_text') || get_field('step10_image_pc') || get_field('step10_image_sp') || get_field('step10_image_description')): ?>
            <div class="p-rcp-textThumb wow fadeIn" data-wow-delay="0.5s">
              <div class="txt">
<?php if(get_field('step10_text')): ?>
                <p class="txt-num10 numbering"><?php the_field('step10_text'); ?></p>
<?php endif; ?>
              </div>
              <figure class="thumb">
                <div class="photo">
<?php 
$image = get_field('step10_image_sp');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->step10_image_sp, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
                </div>
<?php if(get_field('step10_image_description')): ?>
                <figcaption><?php the_field('step10_image_description'); ?></figcaption>
<?php endif; ?>
              </figure>
            </div>
<?php endif; ?>

<?php if(get_field('black_text3')): ?>
            <p class="p-rcp-finishTxt wow fadeIn" data-wow-delay="0.5s"><?php the_field('black_text3'); ?></p>
<?php endif; ?>
            <div class="photo wow fadeIn" data-wow-delay="0.5s">
<?php 
$image = get_field('complete_image_pc');
if($image):
?>
                  <img src="<?php echo $image; ?>" alt="<?php echo get_post_meta( $post->complete_image_pc, '_wp_attachment_image_alt', true ); ?>">
<?php endif; ?>
            </div>
          </div>
        </section>
        
        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->

<?php get_template_part( 'template-parts/post/column' ); ?>

<?php if(get_field('credit')): ?>
        <p class="p-credit p-credit--vol5 wow fadeIn" data-wow-delay="0.5s"><?php the_field('credit'); ?></p>
<?php endif; ?>
        <?php get_template_part('profile'); ?>

        <section class="contents-section contents-section--section3">
          <?php include locate_template('pager.php'); ?>

　        <?php get_template_part('nav-sns'); ?>
        </section>
      </div>
    </main>
    
  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
<?php
//article area end
endwhile;
?>
</html>