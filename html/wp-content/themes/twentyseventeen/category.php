
<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX ,グランエックス,タイガー魔法瓶,土鍋圧力IH炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー、食材のこと">
<meta name="description" content="GRAND X（グランエックス）シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[食材のこと]">
<link rel="shortcut icon" type="image/x-icon" href="/assets/images/common/favicon.ico">

<meta property="og:url" content="https://www.grandx.jp/food/index.html">
<meta property="og:type" content="website">
<meta property="og:title" content="食材のこと｜GX GRANDX クラブ｜タイガー魔法瓶">
<meta property="og:description" content="GRAND X（グランエックス）シリーズ。各地から現生産れた、新鮮で旬な食材についてのご紹介。">
<meta property="og:image" content="">
<meta property="og:site_name" content="食材のこと｜GX GRANDX クラブ｜タイガー魔法瓶">

<title>食材のこと｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="/assets/css/normalize.css">
<link rel="stylesheet" href="/assets/css/common.css">
<link rel="stylesheet" href="/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/accessor/script/typesquare.js?fGhM~60XsvE%3D" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="/assets/js/jquery-1.11.0.min.js"></script>
<script src="/assets/js/breadCrumb.js"></script>
<script src="/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
            <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10&appId=227899304403710";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-53961983-1', 'auto');
      </script>

    
      <div class="header-inner">
        <h1 class="header-logo"><a href="/"><img class="pc" src="/assets/images/common/logo-grandx.png" alt="GRANDX"><img class="sp" src="/assets/images/common/logo-grandx-sp.png" alt="GRANDX"></a></h1>

        <div class="header-nav-btn sp">
          <div class="menu-trigger">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        
        <div class="nav-wrapper open">
          <nav role="navigation" class="header-nav">
            <ul class="nav-container pc">
              <li class="nav-top"><a href="/" onclick="ga('send','event','top','click','PCヘッダナビ＞トップページ');">トップページ</a></li>
              <li class="nav-message"><a href="/message/" onclick="ga('send','event','top','click','PCヘッダナビ＞メッセージ');">メッセージ</a></li>
              <li class="nav-product"><a href="/product/" onclick="ga('send','event','top','click','PCヘッダナビ＞商品アイテム');">商品アイテム</a></li>
              <li class="nav-food"><a href="/food/" onclick="ga('send','event','top','click','PCヘッダナビ＞食材のこと');">食材のこと</a></li>
              <li class="nav-recipe"><a href="/recipe/" onclick="ga('send','event','top','click','PCヘッダナビ＞レシピいろいろ');">レシピいろいろ</a></li>
              <li class="nav-human"><a href="/human/" onclick="ga('send','event','top','click','PCヘッダナビ＞あの人');">あの人</a></li>
              <li class="nav-movie"><a href="/movie/tvcm.html" onclick="ga('send','event','top','click','PCヘッダナビ＞動画ムービー');">動画ムービー</a></li>
              <li class="nav-special"><a href="/special/" onclick="ga('send','event','top','click','PCヘッダナビ＞スペシャル');">スペシャル</a></li>
            </ul>
            <ul class="nav-container sp">
              <li class="nav-top"><a href="/" onclick="ga('send','event','top','click','SPメニュー＞トップページ');">トップページ</a></li>
              <li class="nav-message"><a href="/message/" onclick="ga('send','event','top','click','SPメニュー＞メッセージ');">メッセージ</a></li>
              <li class="nav-product"><a href="/product/" onclick="ga('send','event','top','click','SPメニュー＞商品アイテム');">商品アイテム</a></li>
              <li class="nav-food"><a href="/food/" onclick="ga('send','event','top','click','SPメニュー＞食材のこと');">食材のこと</a></li>
              <li class="nav-recipe"><a href="/recipe/" onclick="ga('send','event','top','click','SPメニュー＞レシピいろいろ');">レシピいろいろ</a></li>
              <li class="nav-human"><a href="/human/" onclick="ga('send','event','top','click','SPメニュー＞あの人');">あの人</a></li>
              <li class="nav-movie"><a href="/movie/tvcm.html" onclick="ga('send','event','top','click','SPメニュー＞動画ムービー');">動画ムービー</a></li>
              <li class="nav-special"><a href="/special/" onclick="ga('send','event','top','click','SPメニュー＞スペシャル');">スペシャル</a></li>
            </ul>
          </nav><!-- // end of nav -->

          <nav role="navigation" class="header-submenu">
            
            <a class="header-btn-sitemap sp" href="/sitemap/">サイトマップ</a>

            <div class="search pc-rayout">
              <script>
                (function() {
                  var cx = '008000047157047102012:hsgzq-mctiu';
                  var gcse = document.createElement('script');
                  gcse.type = 'text/javascript';
                  gcse.async = true;
                  gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                  var s = document.getElementsByTagName('script')[0];
                  s.parentNode.insertBefore(gcse, s);
                })();
              </script>
              <gcse:searchbox-only></gcse:searchbox-only>
            </div>
            
            <p class="header-label-sns sp">「SNS 公式アカウント</p>
            <ul class="submenu-container">
              <li class="icon-search"><p>検索</p></li>
              <li class="icon-sitemap"><a href="/sitemap/" onclick="ga('send','event','top','click','PCヘッダ＞サイトマップ');">サイトマップ</a></li>
              <li class="icon-facebook pc"><a href="https://www.facebook.com/g.x.tiger/?fref=ts" target="_blank" onclick="ga('send','event','top','click','PCヘッダ＞フェイスブックアイコン');">Facebook</a></li>
              <li class="icon-facebook sp"><a href="https://www.facebook.com/g.x.tiger/?fref=ts" target="_blank" onclick="ga('send','event','top','click','SPメニュー＞フェイスブックアイコン');">Facebook</a></li>
              <li class="icon-instagram"><a href="https://www.instagram.com/tigergrandx/" target="_blank" onclick="ga('send','event','top','click','SPメニュー＞インスタグラムアイコン');">Instagram</a></li>
            </ul>
            <a class="icon-tiger" href="https://www.tiger.jp/" target="_blank" onclick="ga('send','event','top','click','PCヘッダ＞タイガーロゴ');">TIGER</a>
          </nav>
        </div>
      </div>
    

      <ol class="local-nav" id="breadCrumb"></ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <main role="main" class="main-content">
      <div class="content-inner detail category-index child">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->

        <h2 class="h2-contents-title">食材のこと</h2>

        <section class="contents-section">
          <ul class="two-column">
            <li class="hover-animation">
              <a href="./story/koshihikari/">
                <img class="bg pc" src="/assets/images/food/thumb01.jpg" alt="">
                <img class="bg sp" src="/assets/images/food/thumb01-sp.jpg" alt="">
                <div class="label">
                  <div class="inner">
                    <p class="sub-copy">食材STORY</p>
                    <p class="font-ica item-name">魚沼産コシヒカリ</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="hover-animation">
              <a href="./story/navel/">
                <img class="bg pc" src="/assets/images/food/thumb02.jpg" alt="">
                <img class="bg sp" src="/assets/images/food/thumb02-sp.jpg" alt="">
                <div class="label">
                  <div class="inner">
                    <p class="sub-copy">食材STORY</p>
                    <p class="font-ica item-name">瀬戸田レモン・瀬戸田ネーブル</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="hover-animation">
              <a href="./story/vegetable/">
                <img class="bg pc" src="/assets/images/food/thumb03.jpg" alt="">
                <img class="bg sp" src="/assets/images/food/thumb03-sp.jpg" alt="">
                <div class="label">
                  <div class="inner">
                    <p class="sub-copy">食材STORY</p>
                    <p class="font-ica item-name">有機野菜・淡路島野菜</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="hover-animation">
              <a href="./story/pudding/">
                <img class="bg pc" src="/assets/images/food/thumb04.jpg" alt="">
                <img class="bg sp" src="/assets/images/food/thumb04-sp.jpg" alt="">
                <div class="label">
                  <div class="inner">
                    <p class="sub-copy">食材STORY</p>
                    <p class="font-ica item-name">糀ぷりん</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </section>
        

        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->
      </div>
    </main>
  </div>

    <footer class="footer">
    <aside class="footer-content">
      <div class="four-column flex">
        <div class="column-block footer-msg">
          <p class="column-inner">食卓に新たな価値を提案するブランド。<br>
〜五感に響きわたる、おいしさと上質感を。〜</p>
        </div>

        <script src="/assets/js/instafeed.min.js"></script>
        <div class="column-block footer-instagram">
          <div class="column-inner">
            
            <p class="footer-sns-title">公式SNSアカウント</p>

            <div class="block-instagram">
              <p class="logo-instagram"><img class="pc" src="/assets/images/common/logo-instagram.png" alt="Instagram"><img class="sp" src="/assets/images/common/logo-instagram-sp.png" alt="Instagram"></p>
              
              <ul class="instagram-thumb-list" id="insta_list">
               
              </ul>
            </div>
          </div>
        </div>

        <div class="column-block footer-facebook">
          <div class="column-inner">
            

            <!-- Facebook -->
            <div class="block-facebook">
              <div class="fb-page" data-href="https://www.facebook.com/g.x.tiger/?fref=ts" data-tabs="timeline" data-width="' + w + '" data-height="' + h + '" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/g.x.tiger/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/g.x.tiger/?fref=ts">GRAND X（グランエックス）</a></blockquote></div>
              
            </div>
            <a class="btn-facebook" href="https://www.facebook.com/g.x.tiger/?fref=ts" target="_blank" onclick="ga('send','event','top','click','フッター＞フェイスブック');"><img class="pc" src="/assets/images/common/btn-facebook.png" alt="Facebook"><img class="sp" src="/assets/images/common/btn-facebook-sp.png" alt="Facebook"></a>
          </div>
        </div>

        <div class="column-block footer-link">
          <ul class="column-inner">
            <li class="footer-link-kenko"><a href="https://oishi-kenko.com/" target="_blank" onclick="ga('send','event','top','click','フッター＞おいしい健康ロゴ');"><img class="pc" src="/assets/images/common/footer-link-kenko.png" alt="おいしい健康"><img class="sp" src="/assets/images/common/footer-link-kenko-sp.png" alt="おいしい健康"></a></li>
            <li class="footer-link-kihon"><a href="https://kurashi-no-kihon.com/" target="_blank" onclick="ga('send','event','top','click','フッター＞くらしのきほんロゴ');"><img class="pc" src="/assets/images/common/footer-link-kihon.png" alt="くらしのきほん"><img class="sp" src="/assets/images/common/footer-link-kihon-sp.png" alt="くらしのきほん"></a></li>
            <li class="footer-link-balloon"><a href="https://balloonfrom.com/" target="_blank" onclick="ga('send','event','top','click','フッター＞BALLONロゴ');"><img class="pc" src="/assets/images/common/footer-link-balloon.png" alt="Balloon"><img class="sp" src="/assets/images/common/footer-link-balloon-sp.png" alt="Balloon"></a></li>
          </ul>
        </div>
      </div>

      <div class="footer-link-tiger"><a href="https://www.tiger.jp/" target="_blank" onclick="ga('send','event','top','click','フッター＞タイガーロゴ');"><img class="pc" src="/assets/images/common/footer-link-tiger.png" alt="TIGER"><img class="sp" src="/assets/images/common/footer-link-tiger-sp.png" alt="TIGER"></a></div>
    </aside>

    <a class="btn-backtop" href="#"><img class="pc" src="/assets/images/common/btn-backtop.png" alt="Balloon"><img class="sp" src="/assets/images/common/btn-backtop-sp.png" alt="Balloon"></a>

    <p class="copyright">Copyright &copy; Tiger Corporation., All Rights Reserved.</p>

  </footer><!-- // end of footer -->
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>