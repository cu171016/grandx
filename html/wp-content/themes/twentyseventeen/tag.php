<?php
// get tag data
$tag = get_queried_object();
$tagname = mb_strtoupper ($tag->name);
?>
<!DOCTYPE HTML>
<html>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QFDV79');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="keywords" content="GX GRANDX クラブ,GX GRANDX ,グランエックス,タイガー魔法瓶,土鍋圧力IH炊飯ジャー,IHホームベーカリー,スチームコンべクションオーブン,コーヒーメーカー,タグ">
<meta name="description" content="GRAND X（グランエックス）シリーズ。食卓に新たな価値を提案するブランド。〜五感に響きわたる、おいしさと上質感を。〜[タグ検索結果]">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/common/favicon.ico">

<meta property="og:url" content="">
<meta property="og:type" content="website">
<meta property="og:title" content="">
<meta property="og:description" content="">
<meta property="og:image" content="">
<meta property="og:site_name" content="">

<title>タグ検索結果｜GX GRANDX クラブ｜タイガー魔法瓶</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">

<script type="text/javascript" src="//typesquare.com/accessor/script/typesquare.js?fGhM~60XsvE%3D" charset="utf-8"></script>
<link href="//fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js"></script>

<!--[if lte IE 8]>
<script type="text/javascript">location.replace( '/ie.html' );</script>
<![endif]-->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QFDV79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="wrap">
    <header role="banner" class="header animated detail">
      <?php get_header(); ?>
      <ol class="local-nav" id="breadCrumb">
        <li><a href="/">トップページ</a></li>
        <li class="active">タグ検索結果</li>
      </ol>
    </header><!-- // end of header -->
    <!--▲▲ /#HEADER ▲▲-->

    <main role="main" class="main-content">
      <div class="content-inner detail tag_search">
        <!-- ▼▼▼　ここから編集可能 ▼▼▼ -->
        <!-- ※パンくずナビの表示は、別途 /assets/js/breadCrumb.js を編集する必要があります-->
        
        <h2 class="h2-contents-title"><span class="tag-title"><?php echo $tagname; ?></span></h2>
        <p class="min-txt">のタグ検索結果を表示しています。</p>
        <section class="contents-section">
            <!-- タグ検索結果 -->
            <ul class="thumb-container flex">
<?php
/* タグに関連する記事を取得 */
$args = array(
  'tag' => $tag->slug,
  'posts_per_page' => -1,
  'orderby' => 'post-date',
);
$the_query = new WP_Query($args);
if ( $the_query->have_posts() ) :
  while ( $the_query->have_posts() ) : $the_query->the_post();
?>
                <li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="<?php the_permalink(); ?>">
<?php 
$image = get_field('tag_image_pc');
if($image):
?>
                        <div class="thumb"><img src="<?php echo $image; ?>" alt=""></div>
<?php endif; ?>

<?php
    if(get_post_meta(get_the_ID(), '_wp_page_template', true) == 'post_template_type_B.php'): // あの人 B おはぎ
?>
                        <h3 class="thumb-title"><?php the_title(); ?> <?php the_field('publish'); ?> <?php the_field('menu'); ?> <?php the_field('who'); ?>さん</h3>
<?php
    elseif(get_post_meta(get_the_ID(), '_wp_page_template', true) == 'post_template_type_C.php' || get_post_meta(get_the_ID(), '_wp_page_template', true) == 'post_template_type_D.php'): // きほんのいいもの・わたしの台所
?>
                        <h3 class="thumb-title"><?php the_title(); ?> 第<?php echo mb_convert_kana(get_field('number'), 'N'); ?>話 <?php the_field('who'); ?>さん</h3>
<?php
    else:
?>
                        <h3 class="thumb-title"><?php the_title(); ?></h3>
<?php
    endif;
?>

                        <p class="thumb-text"><?php the_field('tag_description'); ?></p>
                    </a>
                </li>
<?php
  endwhile;
else :
/*
  http_response_code(301) ;
  header(sprintf('Location: %s', site_url()));
  exit;
*/
endif;
?>
<?php
if ($tagname == '料理') :
?>
			<!--
                <li class="cooking movie grandx thumb-block size-medium hover-animation">
                    <a href="/movie/tvcm.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/tvcm.jpg" alt=""></div>
                        <h3 class="thumb-title">TVCM</h3>
                        <p class="thumb-text">佐々木 希さん出演！TVCM配信中です！テレビで見逃した方は是非ご覧ください。</p>
                    </a>
                </li>
                <li class="cooking knowledge living thumb-block size-medium hover-animation">
                    <a href="/special/nozomi-01/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/nozomi.jpg" alt=""></div>
                        <h3 class="thumb-title">希のとっておき</h3>
                        <p class="thumb-text">グランエックスシリーズを使った、佐々木 希さんのとっておきレシピをご紹介。</p>
                    </a>
                </li>
			--!>                
<?php
elseif ($tagname == '食材') :
?>
<?php
elseif ($tagname == '旬') :
?>
<?php
elseif ($tagname == '道具') :
?>
<?php
elseif ($tagname == 'あの人') :
?>
<?php
elseif ($tagname == '動画') :
?>
<?php
elseif ($tagname == '学び') :
?>
<?php
elseif ($tagname == 'くらし') :
?>
<?php
elseif ($tagname == '健康') :
?>
<?php
elseif ($tagname == 'グランエックス') :
?>
<?php
elseif ($tagname == '料理とレシピ') :
?>
                <li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/jpg/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-001.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜土鍋圧力IH炊飯ジャー（JPG-X100）</h3>
                        <p class="thumb-text">土鍋ご飯が教えてくれること</p>
                    </a>
                </li><li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/kbd/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-002.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜IHホームベーカリー（KBD-X100）</h3>
                        <p class="thumb-text">グルテンフリーの食パンは、ちぎりパンがいちばんおいしい！</p>
                    </a>
                </li><li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/kax/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-003.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜スチームコンべクションオーブン（KAX-X130）</h3>
                        <p class="thumb-text">同時に４枚焼ける究極の絶品トースト</p>
                    </a>
                </li><li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/acq/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-004.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜コーヒーメーカー（ACQ-X020）</h3>
                        <p class="thumb-text">ふたりでうなずいたブラックコーヒー</p>
                    </a>
                </li><li class="cooking knowledge living thumb-block size-medium hover-animation">
                    <a href="/special/nozomi-01/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/nozomi.jpg" alt=""></div>
                        <h3 class="thumb-title">希のとっておき</h3>
                        <p class="thumb-text">グランエックスシリーズを使った、佐々木 希さんのとっておきレシピをご紹介。</p>
                    </a>
                </li><li class="cooking movie grandx thumb-block size-medium hover-animation">
                    <a href="/movie/tvcm.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/tvcm.jpg" alt=""></div>
                        <h3 class="thumb-title">TVCM</h3>
                        <p class="thumb-text">佐々木 希さん出演！TVCM配信中です！テレビで見逃した方は是非ご覧ください。</p>
                    </a>
                </li>

<?php
elseif ($tagname == '旬のお買いもの') :
?>
                <li class="foods season living thumb-block size-medium hover-animation">
                    <a href="/food/story/koshihikari/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/s-001.jpg" alt=""></div>
                        <h3 class="thumb-title">魚沼産コシヒカリ</h3>
                        <p class="thumb-text">従来からの美味しさをそのままに引き継ぐ伝統の南魚沼塩沢産コシヒカリ特別栽培米</p>
                    </a>
                </li><li class="foods season living thumb-block size-medium hover-animation">
                    <a href="/food/story/navel/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/s-002.jpg" alt=""></div>
                        <h3 class="thumb-title">瀬戸田ネーブル</h3>
                        <p class="thumb-text">素材本来の味を楽しむジャムを探して。日本一のレモン・ネーブル生産地が生んだ。</p>
                    </a>
                </li><li class="foods season living thumb-block size-medium hover-animation">
                    <a href="/food/story/vegetable/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/s-003.jpg" alt=""></div>
                        <h3 class="thumb-title">有機野菜</h3>
                        <p class="thumb-text">seasonの野菜7〜9品入り手間をかけて作った美味しい野菜</p>
                    </a>
                </li><li class="foods season living thumb-block size-medium hover-animation">
                    <a href="/food/story/pudding/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/s-004.jpg" alt=""></div>
                        <h3 class="thumb-title">糀ぷりん</h3>
                        <p class="thumb-text">糀を ”新しく”楽しむ、伊勢の老舗醸造元糀屋さんの糀ぷりんです。</p>
                    </a>
                </li><li class="grandx foods season thumb-block size-medium hover-animation">
                    <a href="/special/cp1/">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/c-001.jpg" alt=""></div>
                        <h3 class="thumb-title">グランエックス早期購入キャンペーン</h3>
                        <p class="thumb-text">今なら合計120名様に厳選食材をプレゼント！</p>
                    </a>
                </li>
<?php
elseif ($tagname == 'わが家のキッチン') :
?>
				<li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-001.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜土鍋圧力IH炊飯ジャー（JPG-X100）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-002.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー</h3>
                        <p class="thumb-text">本当においしいものは、つながっていく～土鍋ごはんが毎日ある生活～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-003.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Donabe Style</h3>
                        <p class="thumb-text">甘みと香りがちがう。口に広がる甘みと抜けるような香り。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/story.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-004.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Donabe Story</h3>
                        <p class="thumb-text">「熱」が導く、土鍋革命、タイガー史上最高の本土鍋へ。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-005.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜IHホームベーカリー（KBD-X100）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/switch02.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-006.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー しあわせの記憶編</h3>
                        <p class="thumb-text">～焼きたてのパンの匂いとともに～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-007.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー みんなと一緒がうれしい編</h3>
                        <p class="thumb-text">～家族みんなで食べられるしあわせ～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-008.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Healthy Style</h3>
                        <p class="thumb-text">笑顔ふくらむ、おいしいパン。このパリふわ感が、家族みんなの笑顔のヒミツ。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kax/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-009.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜スチームコンべクションオーブン（KAX-X130）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kax/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-010.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー</h3>
                        <p class="thumb-text">自然のおいしさを、まるごといただく。～家族みんなが笑顔になる時間～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kax/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-011.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Steam Style</h3>
                        <p class="thumb-text">驚きの食感。決め手は”たっぷりスチームのカーボンヒーター”にありました。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/acq/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-012.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜コーヒーメーカー（ACQ-X020）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/acq/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-013.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー</h3>
                        <p class="thumb-text">すべては、このひとときのために。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/acq/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-014.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Press Style</h3>
                        <p class="thumb-text">特別なブラックコーヒーを、お好みで。珈琲職人の技をこの一台に。</p>
                    </a>
                </li>
<?php
elseif ($tagname == 'あの人の暮らし') :
?>
<?php
elseif ($tagname == '暮らしのヒント') :
?>
                <li class="movie living grandx thumb-block size-medium hover-animation">
                    <a href="/movie/story.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/m-001.jpg" alt=""></div>
                        <h3 class="thumb-title">WEBムービー（しあわせスイッチストーリー）</h3>
                        <p class="thumb-text">日々の生活に幸せを感じる、しあわせスイッチストーリーmovieです。</p>
                    </a>
                </li>
<?php
elseif ($tagname == 'グランエックスについて') :
?>
                <li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/jpg/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-001.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜土鍋圧力IH炊飯ジャー（JPG-X100）</h3>
                        <p class="thumb-text">土鍋ご飯が教えてくれること</p>
                    </a>
                </li><li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/kbd/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-002.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜IHホームベーカリー（KBD-X100）</h3>
                        <p class="thumb-text">グルテンフリーの食パンは、ちぎりパンがいちばんおいしい！</p>
                    </a>
                </li><li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/kax/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-003.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜スチームコンべクションオーブン（KAX-X130）</h3>
                        <p class="thumb-text">同時に４枚焼ける究極の絶品トースト</p>
                    </a>
                </li><li class="item knowledge grandx thumb-block size-medium hover-animation">
                    <a href="/product/basic/acq/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/k-004.jpg" alt=""></div>
                        <h3 class="thumb-title">GXのきほん｜コーヒーメーカー（ACQ-X020）</h3>
                        <p class="thumb-text">ふたりでうなずいたブラックコーヒー</p>
                    </a>
                </li><li class="human grandx knowledge thumb-block size-medium hover-animation">
                    <a href="/message/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/me-001.jpg" alt=""></div>
                        <h3 class="thumb-title">メッセージ</h3>
                        <p class="thumb-text">グランエックスからみなさまへのご挨拶</p>
                    </a>
                </li><li class="movie living grandx thumb-block size-medium hover-animation">
                    <a href="/movie/basic.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/m-002.jpg" alt=""></div>
                        <h3 class="thumb-title">WEBムービー（グランエックスのきほん）</h3>
                        <p class="thumb-text">グランエックスの基本的な使い方動画です。</p>
                    </a>
                </li>				<li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-001.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜土鍋圧力IH炊飯ジャー（JPG-X100）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-002.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー</h3>
                        <p class="thumb-text">本当においしいものは、つながっていく～土鍋ごはんが毎日ある生活～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-003.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Donabe Style</h3>
                        <p class="thumb-text">甘みと香りがちがう。口に広がる甘みと抜けるような香り。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/jpg/story.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-004.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Donabe Story</h3>
                        <p class="thumb-text">「熱」が導く、土鍋革命、タイガー史上最高の本土鍋へ。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-005.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜IHホームベーカリー（KBD-X100）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/switch02.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-006.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー しあわせの記憶編</h3>
                        <p class="thumb-text">～焼きたてのパンの匂いとともに～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-007.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー みんなと一緒がうれしい編</h3>
                        <p class="thumb-text">～家族みんなで食べられるしあわせ～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kbd/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-008.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Healthy Style</h3>
                        <p class="thumb-text">笑顔ふくらむ、おいしいパン。このパリふわ感が、家族みんなの笑顔のヒミツ。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kax/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-009.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜スチームコンべクションオーブン（KAX-X130）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kax/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-010.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー</h3>
                        <p class="thumb-text">自然のおいしさを、まるごといただく。～家族みんなが笑顔になる時間～</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/kax/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-011.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Steam Style</h3>
                        <p class="thumb-text">驚きの食感。決め手は”たっぷりスチームのカーボンヒーター”にありました。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/acq/index.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-012.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜コーヒーメーカー（ACQ-X020）</h3>
                        <p class="thumb-text">商品のご紹介</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/acq/switch.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-013.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜しあわせスイッチストーリー</h3>
                        <p class="thumb-text">すべては、このひとときのために。</p>
                    </a>
                </li><li class="grandx item healthy thumb-block size-medium hover-animation">
                    <a href="/product/lineup/acq/style.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/l-014.jpg" alt=""></div>
                        <h3 class="thumb-title">Line up｜Press Style</h3>
                        <p class="thumb-text">特別なブラックコーヒーを、お好みで。珈琲職人の技をこの一台に。</p>
                    </a>
                </li><li class="movie living grandx thumb-block size-medium hover-animation">
                    <a href="/movie/story.html">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/m-001.jpg" alt=""></div>
                        <h3 class="thumb-title">WEBムービー（しあわせスイッチストーリー）</h3>
                        <p class="thumb-text">日々の生活に幸せを感じる、しあわせスイッチストーリーmovieです。</p>
                    </a>
                </li><li class="grandx foods season thumb-block size-medium hover-animation">
                    <a href="/special/cp1/">
                        <div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tags/c-001.jpg" alt=""></div>
                        <h3 class="thumb-title">グランエックス早期購入キャンペーン</h3>
                        <p class="thumb-text">今なら合計120名様に厳選食材をプレゼント！</p>
                    </a>
                </li>
<?php
endif;
?>

            </ul>
        </section>

        <!-- ▲▲▲　ここまで編集可能 ▲▲▲ -->
      </div>
    </main>

    <div class="sp">
    <aside class="nav-sns">
      <ul class="content-inner">
        <li><a class="btn-facebook" href="http://www.facebook.com/share.php?u=https://www.grandx.jp/" onclick="ga('send','event','top','click','下段SNS＞フェイスブックアイコン'); window.open(this.href, 'facebook_share', 'width=550, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/footer-icon-facebook.png" alt="Facebook"></a></li>
        <li><a class="btn-twitter" href="http://twitter.com/share?count=horizontal&text=GRAND%20X%ef%bc%88%e3%82%b0%e3%83%a9%e3%83%b3%e3%82%a8%e3%83%83%e3%82%af%e3%82%b9%ef%bc%89%e3%82%b7%e3%83%aa%e3%83%bc%e3%82%ba%e3%80%82%e9%a3%9f%e5%8d%93%e3%81%ab%e6%96%b0%e3%81%9f%e3%81%aa%e4%be%a1%e5%80%a4%e3%82%92%e6%8f%90%e6%a1%88%e3%81%99%e3%82%8b%e3%83%96%e3%83%a9%e3%83%b3%e3%83%89%e3%80%82%e3%80%9c%e4%ba%94%e6%84%9f%e3%81%ab%e9%9f%bf%e3%81%8d%e3%82%8f%e3%81%9f%e3%82%8b%e3%80%81%e3%81%8a%e3%81%84%e3%81%97%e3%81%95%e3%81%a8%e4%b8%8a%e8%b3%aa%e6%84%9f%e3%82%92%e3%80%82%e3%80%9c&url=https://www.grandx.jp/" onclick="ga('send','event','top','click','下段SNS＞ツイッターアイコン'); window.open(this.href, 'twitter_share', 'width=550, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/footer-icon-twitter.png" alt="Twitter"></a></li>
        <li><a class="btn-line" href="http://line.me/R/msg/text/?Syncer https://www.grandx.jp/" onclick="ga('send','event','top','click','下段SNS＞ラインアイコン'); window.open(this.href, 'line_share', 'width=550, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/common/footer-icon-line.png" alt="LINE"></a></li>
      </ul>
    </aside>


    </div>

  </div>

  <?php get_footer(); ?>
  <!--▲▲ /#FOOTER ▲▲-->
  
</body>
</html>