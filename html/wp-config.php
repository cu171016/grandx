<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'chz2113002_grandx');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'chz2113002');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'YhBGhT5J');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1:3307');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iQ9?N1sK#1_bW ^47l]x7Jj~PydN[SodX!-4:Um!L/zB4!N<u^pBlAR?YQW_^f?m');
define('SECURE_AUTH_KEY',  'R!Bko|{fxqXw.Lek}<v!odvhJej1W&hkeeC)]WsymR`2s,yWcTuiBcIAx.,+d|uE');
define('LOGGED_IN_KEY',    'WxUGd]po?,Y:6^0I7_j<t48]%pnFi*X0!#.uLp#~~OpIhisc_Xq]e4>md1cOumjX');
define('NONCE_KEY',        't(9$ Cd_9hV8fQhL/9@vxdhZ0&e}%#yV56qx``P2Mr-q12/+:aMlsQ.a;46xJT1u');
define('AUTH_SALT',        'TU~I c,K};0+l.!qID&a$01v.[^i1l4hCazq85FB9XOVxg3= @g4=9Kl-waNF/<4');
define('SECURE_AUTH_SALT', ',`PX?LG{~Gzsv%D3(lbA!|5:&@nHAPlvG(^2xyM5 Ct|1(+}Ulp%tFu{@YB#&~][');
define('LOGGED_IN_SALT',   'W5wn@/,XV6iv)G2eU,GT$eFXwV2Q!aumD+=)w/0ex%a}N&gAp2PEh0/wWCpczvkc');
define('NONCE_SALT',       'U;/^T7(+(j(>%lez3Y8yK%NiH*@oOJE33L:^,e(!LwpYVz0uq;fEV2*-FTz1YZJ(');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

