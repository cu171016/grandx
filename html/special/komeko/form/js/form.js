(function($) {
    $(function() {
        //ファイル登録ボタン
        $('#file-button').on('click', function () {
            $('#file-upload').click();
            return false;
        });

        $('#file-upload').on('change', function() {
            //選択したファイル情報を取得し変数に格納
            var file = $(this).prop('files')[0];
            //アイコンを選択中に変更
            //未選択→選択の場合（.filenameが存在しない場合）はファイル名表示用の<div>タグを追加
            //ファイル名を表示
            $('.file-state').html('ファイル名：' + file.name);
        });
    });
})(jQuery);
