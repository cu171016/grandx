$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;
  
  // 構造画像ナビリスト
  GL.$sliderStructure = $('ul#styleStructureImgList');
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  $('#styleTitleSection #styleMvArea p').fadeTo(0, 0);
  
  $('#styleContentsSection #styleExtractionArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleExtractionArea ul li').fadeTo(0, 0);
  $('#styleContentsSection #styleExtractionArea #styleExtractionInner #styleExtractionLeftBox').fadeTo(0, 0);
  $('#styleContentsSection #styleExtractionArea #styleExtractionInner #styleExtractionRightBox').fadeTo(0, 0);

  $('#styleContentsSection #styleTasteArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleTasteArea > p').fadeTo(0, 0);
  $('#styleContentsSection #styleTasteArea #styleTasteInner').fadeTo(0, 0);

  $('#styleContentsSection #styleShapeArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleShapeArea p').fadeTo(0, 0);
  $('#styleContentsSection #styleShapeArea ul li').fadeTo(0, 0);

  $('#styleContentsSection p#styleImg').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);
  $('#contentsWrap p#introBtn').fadeTo(0, 0);
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){
    
    $('#styleTitleSection').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleTitleSection #styleMvArea p').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleExtractionArea ul').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleExtractionArea h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleExtractionArea ul li:eq(0)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleExtractionArea ul li:eq(1)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleExtractionArea #styleExtractionInner #styleExtractionLeftBox').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleExtractionArea #styleExtractionInner #styleExtractionRightBox').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleTasteArea p').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleTasteArea h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleTasteArea > p').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleTasteArea #styleTasteInner').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleShapeArea p, #styleContentsSection #styleShapeArea ul li').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleShapeArea h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleShapeArea p').fadeTo(300, 1);
            $('#styleContentsSection #styleShapeArea ul li').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection p#styleImg').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection p#styleImg').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#inPageBnSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#inPageBnSection ul li').fadeTo(300, 1);
            $('#contentsWrap p#introBtn').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });

});
