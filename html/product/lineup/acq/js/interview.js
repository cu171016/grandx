$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};
    
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  $('#interviewProfileSection').fadeTo(0, 0);
  
  $('#interview01Section h3').fadeTo(0, 0);
  $('#interview01Section p.interviewImg').fadeTo(0, 0);
  $('#interview01Section p.interviewTxt:eq(0)').fadeTo(0, 0);
  $('#interview01Section p.interviewTxt:eq(3)').fadeTo(0, 0);
  $('#interview01Section .interviewArea').fadeTo(0, 0);

  $('#interview02Section h3').fadeTo(0, 0);
  $('#interview02Section .interviewArea .interviewLeftInner').fadeTo(0, 0);
  $('#interview02Section .interviewArea .interviewRightInner').fadeTo(0, 0);

  $('#interview03Section h3').fadeTo(0, 0);
  $('#interview03Section p.interviewImg').fadeTo(0, 0);
  $('#interview03Section p.interviewTxt:eq(0)').fadeTo(0, 0);
  $('#interview03Section .interviewArea').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){
    
    $('#interviewProfileSection').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#interviewProfileSection').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#interview01Section h3').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#interview01Section h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#interview01Section p.interviewImg').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#interview01Section p.interviewTxt:eq(0)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#interview01Section p.interviewTxt:eq(0)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#interview01Section .interviewArea').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#interview01Section p.interviewTxt:eq(3)').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#interview02Section h3').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#interview02Section h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#interview02Section .interviewArea .interviewLeftInner').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#interview02Section .interviewArea .interviewRightInner').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#interview03Section h3').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#interview03Section h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#interview03Section p.interviewImg').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#interview03Section p.interviewTxt:eq(0)').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#interview03Section p.interviewTxt:eq(0)').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#interview03Section .interviewArea').fadeTo(300, 1);
            d.resolve();
            return d;
          });
        deferred.resolve();
      }

    });

    $('#inPageBnSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#inPageBnSection ul li').fadeTo(300, 1);
            $('#contentsWrap p#introBtn').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });

});
