$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  $('#jpgSection').fadeTo(0, 0);
  $('#jpgSection ul.imgList li span').fadeTo(0, 0);

  $('#kbdSection').fadeTo(0, 0);
  $('#kbdSection ul.imgList li span').fadeTo(0, 0);

  $('#kaxSection').fadeTo(0, 0);
  $('#kaxSection ul.imgList li span').fadeTo(0, 0);

  $('#acqSection').fadeTo(0, 0);
  $('#acqSection ul.imgList li span').fadeTo(0, 0);

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){

    $('#jpgSection ul').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#jpgSection').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 1000);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#jpgSection ul.imgList li span').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 2750);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#jpgSection ul.imgList li span').fadeTo(750, 0);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#kbdSection ul').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#kbdSection').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 1000);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#kbdSection ul.imgList li span').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 2750);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#kbdSection ul.imgList li span').fadeTo(750, 0);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#kaxSection ul').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#kaxSection').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 1000);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#kaxSection ul.imgList li span').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 2750);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#kaxSection ul.imgList li span').fadeTo(750, 0);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#acqSection ul').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#acqSection').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 1000);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#acqSection ul.imgList li span').fadeTo(750, 1);
            setTimeout(function(){ d.resolve(); }, 2750);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#acqSection ul.imgList li span').fadeTo(750, 0);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });
  
});
