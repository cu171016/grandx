$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;
  
  // 構造画像ナビリスト
  GL.$sliderStructure = $('ul#styleStructureImgList');
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();

  // YouTubeのsrc取得
  GL.strYtSrc01 = $('#stylePremiumMovieInner iframe').data('yt-id');
  //$('#stylePremiumMovieInner iframe').fadeTo(0, 0);
  GL.strYtSrc02 = $('#styleRippleRightBox iframe').data('yt-id');
  //$('#styleRippleRightBox iframe').fadeTo(0, 0);

  // inviewで表示する要素を非表示に設定
  $('#styleTitleSection #styleMvArea p').fadeTo(0, 0);
  $('#styleContentsSection #styleInfoArea').fadeTo(0, 0);

  $('#styleContentsSection #stylePremiumArea h3').fadeTo(0, 0);
  $('#styleContentsSection #stylePremiumArea p#stylePremiumTxt').fadeTo(0, 0);
  $('#styleContentsSection #stylePremiumArea #stylePremiumMovieInner').fadeTo(0, 0);
  $('#styleContentsSection #stylePremiumArea p#stylePremiumImg').fadeTo(0, 0);

  $('#styleContentsSection #styleStructureArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleStructureArea p#styleStructureTxt').fadeTo(0, 0);
  $('#styleContentsSection #styleStructureArea #styleStructureInner').fadeTo(0, 0);
  $('#styleContentsSection #styleStructureArea ul#styleStructureNaviList li').fadeTo(0, 0);

  $('#styleContentsSection #styleEnzymeArea h3').fadeTo(0, 0);

  $('#styleContentsSection #styleRippleArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleRippleArea #styleRippleInner').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);
  $('#contentsWrap p#introBtn').fadeTo(0, 0);
  
  // slick初期化
  GL.$sliderStructure.slick({
    autoplay:false,
    arrows:false,
    infinite:false,
    speed:500
  });

  // inviewイベント設定
  $('#styleTitleSection').one('inview', function(event, isInView){

    if(isInView){
      
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#styleTitleSection #styleMvArea p').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleInfoArea').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 0);
          return d;
        });
      deferred.resolve();
    }

  });

  $('#styleContentsSection #stylePremiumArea').one('inview', function(event, isInView){

    if(isInView){
      
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #stylePremiumArea h3').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #stylePremiumArea p#stylePremiumTxt').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #stylePremiumArea #stylePremiumMovieInner').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 1000);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #stylePremiumArea p#stylePremiumImg').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 0);
          return d;
        });
      deferred.resolve();
    }

  });

  $('#styleContentsSection #styleStructureArea').one('inview', function(event, isInView){

    if(isInView){
      
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleStructureArea h3').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleStructureArea p#styleStructureTxt').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleStructureArea #styleStructureInner').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleStructureArea ul#styleStructureNaviList li').velocity({ opacity:1 }, { duration:500, queue:false });
          setTimeout(function(){ d.resolve(); }, 0);
          return d;
        });
      deferred.resolve();
    }

  });

  $('#styleContentsSection #styleEnzymeArea h3').one('inview', function(event, isInView){

    if(isInView){
      
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleEnzymeArea h3').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          //$('#styleContentsSection #styleEnzymeArea p#styleEnzymeImg').fadeTo(750, 1);
          setTimeout(function(){ d.resolve(); }, 0);
          return d;
        });
      deferred.resolve();
    }

  });

  $('#styleContentsSection #styleRippleArea #styleRippleInner').one('inview', function(event, isInView){

    if(isInView){
      
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleRippleArea h3').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#styleContentsSection #styleRippleArea #styleRippleInner').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 0);
          return d;
        });
      deferred.resolve();
    }

  });

  $('#inPageBnSection').one('inview', function(event, isInView){

    if(isInView){
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#inPageBnSection ul li').velocity({ opacity:1 }, { duration:300, queue:false });
          $('#contentsWrap p#introBtn').velocity({ opacity:1 }, { duration:300, queue:false });
          d.resolve();
          return d;
        });
      deferred.resolve();
    }

  });

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){

    // YouTubeのsrc設定
    $('#stylePremiumMovieInner iframe').attr('src', 'https://www.youtube.com/embed/' +GL.strYtSrc01 + '?rel=0');
    //$('#stylePremiumMovieInner iframe').fadeTo(0, 1);
    $('#styleRippleRightBox iframe').attr('src', 'https://www.youtube.com/embed/' +GL.strYtSrc02 + '?rel=0');
    //$('#styleRippleRightBox iframe').fadeTo(0, 1);

  });

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 構造画像リストbeforeChangeイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  GL.$sliderStructure.on('beforeChange', function(slick, currentSlide, nextSlide, animSlide){
    
    var iCurrenIdx = animSlide - 1;

    $('ul#styleStructureNaviList li').children('a').children('img').removeClass('current');

    $('ul#styleStructureNaviList li').each(function(){
      $(this).children('a').children('img').attr('src', $(this).children('a').children('img').attr('src').replace('_on', '_off'));
    });

    if(iCurrenIdx === -1){
      $('ul#styleStructureArrowsList li#styleStructureArrowsPrev').hide();
      return;
    }
    else if(iCurrenIdx === 5){
      $('ul#styleStructureArrowsList li#styleStructureArrowsPrev').show();
      $('ul#styleStructureArrowsList li#styleStructureArrowsNext').hide();
    }
    else{
      $('ul#styleStructureArrowsList li#styleStructureArrowsPrev').show();
      $('ul#styleStructureArrowsList li#styleStructureArrowsNext').show();
    }

    $('ul#styleStructureNaviList li').eq(iCurrenIdx).children('a').children('img').addClass('current');
    $('ul#styleStructureNaviList li').eq(iCurrenIdx).children('a').children('img').attr('src', $('ul#styleStructureNaviList li').eq(iCurrenIdx).children('a').children('img').attr('src').replace('_off', '_on'));

  });

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 構造画像ナビリストclickイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $('ul#styleStructureNaviList li a').click(function(){
    
    // クリックしたナビ(ボタン)に応じてスライド
    GL.$sliderStructure.slick('slickGoTo', $(this).parent('li').index() + 1);
    
    return false;

  });

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 構造画像アローリスト戻るclickイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $('ul#styleStructureArrowsList li#styleStructureArrowsPrev a').click(function(){
    
    // 1つ戻る
    GL.$sliderStructure.slick('slickPrev');
    
    return false;

  });

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 構造画像アローリスト進むclickイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $('ul#styleStructureArrowsList li#styleStructureArrowsNext a').click(function(){
    
    // 1つ進む
    GL.$sliderStructure.slick('slickNext');
    
    return false;

  });

});
