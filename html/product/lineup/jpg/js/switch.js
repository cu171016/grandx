$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;
    
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();
  
  // YouTubeのsrc取得
  GL.strYtSrc = $('#switchTitleMovieInner iframe').data('yt-id');

  // inviewで表示する要素を非表示に設定
  $('#switchKeyword01Section #switchKeyword01_01Area').fadeTo(0, 0);
  $('#switchKeyword01Section #switchKeyword01_01Area h3').fadeTo(0, 0);
  $('#switchKeyword01Section #switchKeyword01_02Area').fadeTo(0, 0);
  $('#switchKeyword01Section #switchKeyword01_02Area #switchKeyword01_02Inner').fadeTo(0, 0);

  $('#switchKeyword02Section').fadeTo(0, 0);
  $('#switchKeyword02Section h3').fadeTo(0, 0);
  $('#switchKeyword02Section #switchKeyword02Area').fadeTo(0, 0);

  $('#switchKeyword03Section').fadeTo(0, 0);
  $('#switchKeyword03Section h3').fadeTo(0, 0);
  $('#switchKeyword03Section ul li').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);

  // inviewイベント設定
  $('#switchKeyword01Section #switchKeyword01_01Area').one('inview', function(event, isInView){

    if(isInView){

        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#switchKeyword01Section #switchKeyword01_01Area').velocity({ opacity:1 }, { duration:300, queue:false });
            if($('body').hasClass('spDevice')){
              $('#switchKeyword01Section #switchKeyword01_01Area h3').velocity({ opacity:1 }, { duration:300, queue:false });
            }
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            if(!$('body').hasClass('spDevice')){
              $('#switchKeyword01Section #switchKeyword01_01Area h3').velocity({ opacity:1 }, { duration:300, queue:false });
            }
            d.resolve();
            return d;
          });
        deferred.resolve();

    }

  });

  $('#switchKeyword01Section #switchKeyword01_02Area').one('inview', function(event, isInView){

    if(isInView){
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#switchKeyword01Section #switchKeyword01_02Area').velocity({ opacity:1 }, { duration:300, queue:false });
          if($('body').hasClass('spDevice')){
            $('#switchKeyword01Section #switchKeyword01_02Area #switchKeyword01_02Inner').velocity({ opacity:1 }, { duration:300, queue:false });
          }
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          if(!$('body').hasClass('spDevice')){
            $('#switchKeyword01Section #switchKeyword01_02Area #switchKeyword01_02Inner').velocity({ opacity:1 }, { duration:300, queue:false });
          }
          d.resolve();
          return d;
        });
      deferred.resolve();
    }

  });

  $('#switchKeyword02Section').one('inview', function(event, isInView){

    if(isInView){
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#switchKeyword02Section').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#switchKeyword02Section h3').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#switchKeyword02Section #switchKeyword02Area').velocity({ opacity:1 }, { duration:300, queue:false });
          d.resolve();
          return d;
        });
      deferred.resolve();
    }

  });

  $('#switchKeyword03Section').one('inview', function(event, isInView){

    if(isInView){
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#switchKeyword03Section').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#switchKeyword03Section h3').velocity({ opacity:1 }, { duration:300, queue:false });
          setTimeout(function(){ d.resolve(); }, 550);
          return d;
        })
        .then(function(){
          var d = $.Deferred();
          $('#switchKeyword03Section ul li').velocity({ opacity:1 }, { duration:300, queue:false });
          d.resolve();
          return d;
        });
      deferred.resolve();
    }

  });

  $('#inPageBnSection').one('inview', function(event, isInView){

    if(isInView){
      var deferred = $.Deferred();
      deferred
        .then(function(){
          var d = $.Deferred();
          $('#inPageBnSection ul li').velocity({ opacity:1 }, { duration:300, queue:false });
          d.resolve();
          return d;
        });
      deferred.resolve();
    }

  });
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){

    // YouTubeのsrc設定
    $('#switchTitleMovieInner iframe').attr('src', 'https://www.youtube.com/embed/' +GL.strYtSrc + '?rel=0');

  });

});
