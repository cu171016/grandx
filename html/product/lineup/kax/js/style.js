$(function(){

  'use strict';

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 定数・グローバル変数
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  
  var GL = {};  // グローバル変数用配列
  
  // ユーザーエージェント情報
  GL.ua = {};

  GL.bDeffed01 = false;
  GL.bDeffed01Timer = 0;
    
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // イベントハンドラ
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // 初期処理
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

  // ユーザーエージェント取得
  GL.ua = $.fn.getUaInfo();
    
  $('#styleTitleSection #styleMvArea p').fadeTo(0, 0);
  
  $('#styleContentsSection #styleHeaterArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleHeaterArea p#styleHeaterTxt').fadeTo(0, 0);
  $('#styleContentsSection #styleHeaterArea p#styleHeaterImg').fadeTo(0, 0);
  $('#styleContentsSection #styleHeaterArea ul').fadeTo(0, 0);
  $('#styleContentsSection #styleHeaterArea #styleHeaterInner').fadeTo(0, 0);

  $('#styleContentsSection #styleOperabilityArea h3').fadeTo(0, 0);
  $('#styleContentsSection #styleOperabilityArea p').fadeTo(0, 0);
  $('#styleContentsSection #styleOperabilityArea ul').fadeTo(0, 0);

  $('ul#styleImgList li').fadeTo(0, 0);

  $('#inPageBnSection ul li').fadeTo(0, 0);
  $('#contentsWrap p#introBtn').fadeTo(0, 0);
  
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  // loadイベント
  //ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  $(window).load(function(){
    
    $('#styleTitleSection').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleTitleSection #styleMvArea p').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleHeaterArea p#styleHeaterImg, #styleContentsSection #styleHeaterArea ul').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleHeaterArea h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleHeaterArea p#styleHeaterTxt').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleHeaterArea p#styleHeaterImg').fadeTo(300, 1);
            $('#styleContentsSection #styleHeaterArea ul').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleHeaterArea #styleHeaterInner').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#styleContentsSection #styleOperabilityArea p, #styleContentsSection #styleOperabilityArea ul').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleOperabilityArea h3').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 550);
            return d;
          })
          .then(function(){
            var d = $.Deferred();
            $('#styleContentsSection #styleOperabilityArea p').fadeTo(300, 1);
            $('#styleContentsSection #styleOperabilityArea ul').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('ul#styleImgList li').one('inview', function(event, isInView){

      if(isInView){
        
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('ul#styleImgList li').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

    $('#inPageBnSection').one('inview', function(event, isInView){

      if(isInView){
        var deferred = $.Deferred();
        deferred
          .then(function(){
            var d = $.Deferred();
            $('#inPageBnSection ul li').fadeTo(300, 1);
            $('#contentsWrap p#introBtn').fadeTo(300, 1);
            setTimeout(function(){ d.resolve(); }, 0);
            return d;
          });
        deferred.resolve();
      }

    });

  });

});
